[comment]: <> (------------------------------------------------)
[comment]: <> (File is used to autogenerate final release-notes)
[comment]: <> (------------------------------------------------)
[comment]: <> (These lines are updated by script, do not edit !)
##### Version: ##RELEASE_VERSION##
##### Release date: ##RELEASE_DATE##
###### Summary of changes
[comment]: <> (Edit this section as needed for next release)
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|n/a|n/a|

[comment]: <> (Previous release history - do not edit below)


---
##### Version: 1.1.4
##### Release date: 2019.03.21-16:45
###### Summary of changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|AFRR-4172|no|Added log info for registered stop|
|n/a|no|made content-type check a bit more robust|
|n/a|no|removed stacktrace details from json error response|
|n/a|no|solver fails for requests where content-type header contains charset specifier|
|n/a|no|stop signal is not registered until initialization of model object|




---
##### Version: 1.1.3
##### Release date: 2019.03.07-16:48
###### Summary of changes

|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|Yes|Improved error handling and logging (details below)|
|n/a|No|Correlation id support for improved logging of operations|
|n/a|No|Added constraints, objective and json response function|
|n/a|No|Added default values for optimization parameters|
|n/a|No|Object oriented refactoring of code for improved maintenance|
|n/a|No|Added stop callback on model|
|n/a|No|Added several unittests|
|n/a|No|Appends only non-zero solution values for relaxed variables|
|n/a|No|Fixed down flow constraint|

#### Improved error handling and logging

The logging and error handling in afrr-solver has been updated such that
1. All error situations are logged to afrr_solver.log
2. All errors are returned as a proper json and a HTTP response to the client

Errors caused by faulty requests from Fifty are logged as warnings only in the solver application, as these shall not trigger monitoring systems in production.

A json error response will always contain these fields

|field|Description|Example|
|-----|-----------|-------|
|status|Hardcoded to FAILED|FAILED|
|error|error type, one these: <p> ImplementationError,InputError,SystemError,UnknownError|UnknownError|
|message|The error message|400 Bad Request: The browser (or proxy) sent a request that this server could not understand.|
|details|A stacktrace from python interpreter|...|




---
##### Version: 1.1.2
##### Release date: 2019.02.25-11:53
###### Summary of changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|AFRR-3465|no|changed flow variables to integer in order to fix all reservation of CZC to 1 MW steps|
|AFRR-3768|no|changed the order of out and in for CZC cap and cost|



---
##### Version: 1.1.1
##### Release date: 2019.01.31-13:23
###### Changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|AFRR-2943|No|increased number of decimals for optimalityGap output|
|AFRR-3281|No|changed relaxed output to show 'new demand', not relaxed value|




---
##### Version: 1.1.0
##### Release date: 2019.01.23-17:59
##### Changes

In addition to the tickets below this release has the following changes

###### Property file with environment variables
The release now contains the file "solver.properties". Edit the file prior to starting the service in order to override settings for
* debug mode for Flask server (default is false)
* multithreading mode for Flask server (default is false)
* port number of solver service (default is 6000)

###### Checksum for release artefact
The zip file is now distributed with an additional sha256 checksum file named afrr-solver-x.y.z.sha256. The file can be used to verify the zip file as follows:
```bash
 sha256 -c afrr-solver-x.y.z.zip
```

###### Tickets
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|yes|changed alg according to new structure on CZC steps and rules on CZC cost|
|n/a|no|small text adjustments to error logging|
|n/a|no|release now comes with sha256 checksum|
|n/a|no|flask threading can now be enabled using environment variable|
|AFRR-3201,AFRR-2956|no|changed output for FAIL status|



---
##### Version: 1.0.0
##### Release date: 2019.01.17-15:46
###### Summary of changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|Yes|Added totalCost in output JSON|
|n/a|Yes|id in json output added|
|AFRR-2943|No|Added cut to reduce solution space and speed up algorithm|
|n/a|No|Fixed zero tolerance bug for solution status|


---
##### Version: 0.0.16
##### Release date: 2019.01.10-09:08

###### Changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|yes|Added get_status endpoint and terminate endpoint|
|n/a|no|added two more soft constraints and changed relaxed output lists|


---
##### Version: 0.0.15
##### Release date: 2019.01.02-16:06
###### Summary of changes

* progress and termination endpoints


###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|Yes|add endpoint for terminating a running task|
|n/a|Yes|Add temporary results to termination endpoint|
|n/a|Yes|include preliminary result in result endpoint|
|AFRR-1794|No|Algorithm progress|
|AFRR-1793|No|Algorithm termination|



---
##### Version: 0.0.14
##### Release date: 2018.12.04-13:20
###### Summary of changes
There are no API changes in this release

_Functional_
* Fixed a bug in the status field for SUB vs OPT

_Technical_
* heartbeat/ endpoint now also displays application version number
* application logging now working, following logs are created
  * stoppeklokke.log (timed operations)
  * sys.log  (start/stop boot messages)
  * afrr-solver.log (general log messages)

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|n/a|n/a|


---
##### Version: 0.0.13
##### Release date: 2018.11.30-08:06
###### Summary of changes
_Functional_
* MMS14-1502 - Configuration of stepwise gap functionality available
* changed the demand constraint to ensure that a solution is found despite of lov marked volume

_Technical_
* Added log object and optimization duration to json response. Updated status field

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|MMS14-1502|yes|Configuration of stepwise gap functionality available|


---
##### Version: 0.0.12
##### Release date: 2018.11.19-11:25
###### Summary of changes
_Functional_
* n/a

_Technical_
* improvements to stoppeklokke log

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|MMS14-1490|No|Added logging of algorithm steps to stoppeklokke log|


---
##### Version: 0.0.11
##### Release date: 2018.11.16-13:48
###### Summary of changes
_Functional_
* ALGIMP - fixed type bug in logger

_Technical_
* added config for stoppeklokke log

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|n/a|n/a|


---
##### Version: 0.0.10
##### Release date: 2018.11.16-10:56
###### Summary of changes
_Functional_
* ...

_Technical_
* Upgraded Xpress to v8.5.8 with fix for instability issues
* Added logging support
* Improved error handling
* Deactivated Flask threading, multiple solvers can no longer run simultanously

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|n/a|n/a|n/a|


---
##### Version: 0.0.9
##### Release date: 2018.11.09-15:00
###### Summary of changes
_Functional_
* Decimal handling

_Technical_
* Support for autogen of releasenotes

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|MGMT-218|nei|removed decimals in output for parameters and reduced number of decimals for variables|


---
##### Version: 0.0.8
##### Release date: 2018.11.06-18:51
###### Summary of changes
* We discovered a bug in the FICO Xpress use of Sum. We have now reformulated the code to avoid these calls and avoid the crashes. We still don’t know why it crashed, but the issues has been reported to FICO

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|AFRR-2137|No|changed call to xpress to avoid crashing|


---
##### Version: 0.0.4
##### Release date: 31.10.2018

###### Summary of changes
* Performance fixes

###### Functional changes
|Ticket|API change?|Comment|
|:------:|:-----------:|:-------|
|AFRR-2138|No|Fixed performance issue|
