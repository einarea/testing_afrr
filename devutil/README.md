###### NOTE: Files in this directory are used by Optimeering only, for their local development. The fifty program uses its own docker infrastructure for the purpose of deploying fcr/afrr solvers to systest environments. 
  
# aFRR Optimization

## Description

## Development

From the development branch, create a new branch and after the changes have been made, merge back into the development branch.

## Running

In the top folder, run:

```bash
docker-compose up
```