import os, sys
from setuptools import setup

if 'SOLVER_VERSION' not in os.environ:
    print ("Environment variable SOLVER_VERSION must define the version to build/release")
    sys.exit(1)

print('Using SOLVER_VERSION={}'.format(os.environ.get('SOLVER_VERSION')))

# Do not edit version in this file manually. Version variable is passed as arg from Makefile
setup(
    include_package_data=True,
    name = 'afrr_solver',
    version = os.environ.get('SOLVER_VERSION'),
    description = ('Web tjeneeste som implementerer aFRR solver'),
    url = 'ssh://git@bitbucket.larm.statnett.no:7999/aof/afrr-solver.git',
    author = 'Fifty',
    author_email = 'fifty@statnett.no',
    packages = [ 'afrr_solver' ],
    install_requires = [ 'xpress', 'Flask', 'flask_api', 'numpy', 'pandas'],
)

