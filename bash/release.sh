#!/usr/bin/env bash
#
# Hjelpescript som benyttes av make for å lage git releaser
# - Finner forslag til ny versjon basert på git loggen og prompter for ny versjon
# - Lager release på egen branch og merger tilbake til master
# - Genererer release notes
#

# EXAMPLE   ------------->    # RESULT
# increment_version 1         # 2
# increment_version 1.0.0     # 1.0.1
# increment_version 1 2       # 1.1
# increment_version 1.1.1 2   # 1.2
function increment_version() {
 local v=$1
 if [ -z $2 ]; then
    local rgx='^((?:[0-9]+\.)*)([0-9]+)($)'
 else
    local rgx='^((?:[0-9]+\.){'$(($2-1))'})([0-9]+)(\.|$)'
    for (( p=`grep -o "\."<<<".$v"|wc -l`; p<$2; p++)); do
       v+=.0; done; fi
 val=`echo -e "$v" | perl -pe 's/^.*'$rgx'.*$/$2/'`
 echo "$v" | perl -pe s/$rgx.*$'/${1}'`printf %0${#val}s $(($val+1))`/
}

function previous_release_tag() {
    local tag=$(git tag --sort=v:refname | grep release | tail -1)
    echo "$tag"
}

function show_algoritm_updates() {
    local cmd='git log $(previous_release_tag)..HEAD --oneline --format="%s" --no-merges | grep -iE "^(ALGIMP|ALGAPI)" | sort -u'
    if [[ $(eval $cmd | wc -l) == "0" ]]; then
        echo None
    else
        eval $cmd
    fi
}

function previous_release() {
    local version=$(previous_release_tag | cut -d "-" -f 2)
    echo "$version"
}

function next_release() {
    local next="0.0.1"
    local prev=$(previous_release)
    if [ "$prev" != "" ]; then
        next=$(increment_version "$prev")
    fi

    echo "$next"
}

function prepare_release() {
    local VERSION=${1?'prepare_release: version input parameter undefined, aborting...'}
    BRANCH=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')

    if [ $BRANCH != "master" ]; then
        echo "Release must be performed from master branch"; echo
        exit 1
    fi

    echo "Checking if we are up to date with remotes"
    git remote update

    for remote in $(git remote); do
        incoming=$(git log HEAD..$remote/master --oneline | wc -l)
        if [ "$incoming" != "0" ]; then
            echo "Remote $remote has $incoming incoming changes, aborting"
            exit 1
        fi
    done

    echo "Checking for uncommitted changes"
    if [[ $(git status --porcelain) ]]; then
      echo "There are uncommitted changes, aborting."; echo
      exit 1
    fi

    local PREVIOUS=$(previous_release)

    echo "Previous release was $PREVIOUS, now creating release: $VERSION"

    #
    # Create untracked branch for performing changes related to release
    #
    RELEASE_BRANCH=release-$VERSION
    git checkout -b $RELEASE_BRANCH

    TS=$(date "+%Y.%m.%d-%H:%M")

    # Generating release-notes.tmp which make will copy to release artefact (ignored by git)
    cat "RELEASE-NOTES-template.md" | grep -vE "^\[comment\]" | sed "s|##RELEASE_VERSION##|$VERSION|g" | sed "s|##RELEASE_DATE##|$TS|g" > dist/release-notes.tmp

    # Update template for next release cycle
    cat bash/release-notes-header.md dist/release-notes.tmp > RELEASE-NOTES-template.md

    echo "Committing release branch"
    git commit -am "Releasing version : $VERSION"
}

#
# Only invoked if release has been successfully uploaded to nexus
#
function finalize_release() {
    RELEASE_BRANCH=$(git branch | grep \* | cut -d ' ' -f2 | grep release)
    [ -z ${RELEASE_BRANCH} ] && echo "error: not on release branch" && exit 1

    echo "Merging release branch to master"
    git checkout master
    git merge --ff-only $RELEASE_BRANCH

    echo "Tagging master with $RELEASE_BRANCH"
    git tag $RELEASE_BRANCH

    echo "Pushing to remotes"
    git remote | xargs -L1 -I R git push --tags R master

    echo "Deleting release branch"
    git branch -d $RELEASE_BRANCH

    echo "Done"
}


