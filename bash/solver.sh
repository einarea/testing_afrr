#!/usr/bin/env bash
#
# Hjelpe-script for å starte opp tjenesten
# Scriptet er identisk for FCR og aFRR tjenestene !!
#
# Brukes både ved oppstart i AT/produksjon og av docker containere i devtest/systest
#
# MERK:
#    Scriptet scanner stdout for FICO bootstrap melding
#    Dersom FICO output omdirigeres til callback for vanlig applogging må dette scriptet korrigeres tilsvarende
#
ME=$(basename $0)
APP_NAME="${ME%.*}"
WORKING_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
LOG_DIR=$WORKING_DIR/log
APP_VERSION=$(basename $WORKING_DIR)
INSTALL_DIR=$(cd "$WORKING_DIR/.." && pwd)
PID_FILE="$INSTALL_DIR/.pid"
SOLVER_PID=$(find "$PID_FILE" >/dev/null 2>&1 && cat $PID_FILE)
PYTHON_CMD=python
PROG=${APP_NAME}-starter.py
cd ${WORKING_DIR}

BOOT_MESSAGE="started|failed|running[[:space:]]on[[:space:]]http"

mkdir -p $LOG_DIR
SYSLOG=${SYSLOG:-"$LOG_DIR/sys.log"}

function stop() {
  local pid="$1"
  if [ ! "$pid" ]; then
    echo "Fant ikke pid, fortsetter"
    return 0;
  fi

  for signal in SIGTERM SIGINT SIGKILL
  do
    kill -${signal} ${pid} >/dev/null 2>&1

    for i in `seq 10`
    do
      kill -0 ${pid} >/dev/null 2>&1 || break
      echo -n "."
      sleep 1
    done
    kill -0 ${pid} >/dev/null 2>&1 || break
    echo -n "*"
  done

  if kill -0 $pid > /dev/null 2>&1;  then
        echo -e "\n`date --iso-8601=seconds` **** Klarte ikke stoppe $APP_NAME ($pid)****"
        return 1;
  fi

  /bin/rm -f ${PID_FILE?}
  echo -e "\n`date --iso-8601=seconds` **** $ME stoppet **** " | tee -a $SYSLOG
  return 0
}

function start() {
  touch $SYSLOG
  nohup $PYTHON_CMD $PROG "$@" >> $SYSLOG 2>&1 &
  checkIsStarted $!
}

function checkIsStarted(){
  local pythonpid=${1?'mangler pid'}
  tail -f -n0 --pid=$$ $SYSLOG | grep -qEi "$BOOT_MESSAGE" &
  local grep_pid=$!

  for i in `seq 10`
  do
    kill -0 $grep_pid >/dev/null 2>&1
    if [ $? -eq 1 ]
    then
      melding=`tac ${SYSLOG} | grep -m1 -Ei "$BOOT_MESSAGE"`
      if [[ $melding =~ .*Running[[:space:]]on[[:space:]]http.* ]]
      then
          echo $pythonpid > ${PID_FILE?}
          echo -e "\n`date --iso-8601=seconds`  **** Suksess! startet $APP_NAME ($pythonpid) ****" | tee -a $SYSLOG
          return 0
      else
          echo -e "\n`date --iso-8601=seconds`  **** Klarte ikke starte $APP_NAME:  **** \nFeil = $melding" | tee -a $SYSLOG
          stop ${pythonpid}
          exit 1;
      fi
    fi

    # in case python process dies for other reasons
    kill -0 $pythonpid >/dev/null 2>&1
    if [ $? -eq 1 ]
    then
        echo -e "\n`date --iso-8601=seconds`  **** Klarte ikke starte $APP_NAME:  **** " | tee -a $SYSLOG
        exit 1;
    fi

    sleep 1
    echo -n "."
  done

  # timed out
  echo -e "\n`date --iso-8601=seconds` **** Klarte ikke starte $APP_NAME. **** " | tee -a $SYSLOG
  stop ${pythonpid}
  exit 1
}


case "$1" in
  'start')
    kill -0 $SOLVER_PID 2>/dev/null && echo "$ME er ikke stoppet (pid=$SOLVER_PID)" && exit 1

    # Vi forutsetter at riktig kjøremiljø er aktivert
    if [ -z $VIRTUAL_ENV ]; then
        echo "Du må først aktivere virtuelt miljø med 'source ~/.virtualenvs/$APP_VERSION/bin/activate'"
        exit 1
    fi
    if [ "$(basename $VIRTUAL_ENV)" != "$APP_VERSION" ] && [ "$(basename $VIRTUAL_ENV)" != "virtual" ]; then
	echo "Aktivt virtuelt miljø er '$(basename $VIRTUAL_ENV)', men du står i directory for '$APP_VERSION'"
        exit 1
    fi
    shift;
    # Hindre at flask ber om debug-pin dersom xpress tryner
    if [ -f $WORKING_DIR/solver.properties ]; then
        echo "Using environment from $WORKING_DIR/solver.properties"
        source $WORKING_DIR/solver.properties
    fi
    start "$@"
    ;;
  'stop')
    stop $SOLVER_PID
    ;;
  *)
    echo "Usage: $0 [start|stop]"
    ;;
esac


