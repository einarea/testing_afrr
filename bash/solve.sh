#!/usr/bin/env bash
#
# Hjelpescript for å poste en json request fil fcr/affr solver på angitt url:port
#
ME=$(basename "$0")

function usage() {
    echo "Usage: $ME -r <file.json> [-p <port>]"
    echo "  -r : use given file as request"
    echo "  -u : url for fico service, default http://localhost"
    echo "  -p : port number for fico service, default 5000"
    exit 0
}

PORT=5000
URL="http://localhost"

while getopts :r:u:p: opt; do
  case $opt in
    r)
	REQUEST_FILE=$OPTARG
	;;
    u)
	URL=$OPTARG
	;;
    p)
	PORT=$OPTARG
	;;
    \?)
	echo "ugyldig opsjon: -$OPTARG"
	usage
	;;
    :)
	echo "Mangler parameter for -$OPTARG"
	usage
	;;
    *)
	echo "error"
	usage
	;;
  esac
done

if [ -z ${REQUEST_FILE+x} ];then
    usage
    exit 1
fi

curl -X POST --data @$REQUEST_FILE -H "Content-Type: application/json" -v "$URL:$PORT/solve"
