# afrr-solver 

### Generelt
afrr-solver implementeres og leveres av det norske selskapet Optimeering.  

Programmodulen utvikles i Python og består av to deler
- En web-tjeneste som eksponerer /solve endepunktet som skal kalles av Damas
- En implementasjon for å løse problemet

### Versjonskontroll

For øyeblikket (sept 2018) versjonsstyres dette prosjektet i Optimeering sitt BitBucket repository. Tilgang gis til Fifty medarbeidere etter behov.

```sh   
git@bitbucket.org:optimeeringdev/afrr.git
```

I tillegg er repository'et speilet til følgende repo i Fiftys BitBucket instans 

```sh
git@bitbucket.larm.statnett.no:7999/aof/afrr-solver.git
``` 

Det er laget en Makefile som:<br>

    - gjør det enkelt å sette opp lokalt utviklingsmiljø  
    - gjør det enkelt å opprette å versjonere nye releaser samt publisere disse til Nexus

#### Oppsett av lokalt git repo
Synk av kode mellom Optimeerings og Fiftys bitbucket repos kan settes opp som beskrevet under. For å klone fra Optimeering sitt repo må Optimeering gi tilgang til en BitBucket cloud bruker med din statnett epost adresse   

1. klon optimeerings repository fra bitbucket.org med remote navn "optimeering"
    ```sh
    git clone https@bitbucket.org:optimeeringdev/afrr.git
    git remote rename origin optimeering
    ```
 
2. Add en remote mot fiftys kopi i BitBucket og gi den navnet "origin"
    ```sh   
    git remote add origin ssh://git@bitbucket.larm.statnett.no:7999/aof/afrr-solver.git
    # Dine remotes bør nå se slik ut
    $ git remote -vv
    optimeering	https://fifty_erik:<secret>@bitbucket.org/optimeeringdev/afrr.git (fetch)
    optimeering	https://fifty_erik:<secret>@bitbucket.org/optimeeringdev/afrr.git (push)
    origin	ssh://git@bitbucket.larm.statnett.no:7999/aof/afrr.git (fetch)
    origin	ssh://git@bitbucket.larm.statnett.no:7999/aof/afrr.git (push)
    ```

#### Arbeidsprosess

Typisk arbeidsprosess for utvikling av afrr-solver<p>
1. Optimeering committer endringer på master branch i sitt BitBucket cloud repo
2. Fifty prosjektet henter ned endringene
    ```sh
    git fetch optimeering
    git pull optimeering master 
    ```
3. Endringene replikeres til Fiftys repository
    ```sh
    git push origin master
    ```
 
4. Ny release klargjøres (detaljer lenger ned) : 
   - prosjektet bygger ny release og tagger git repository med "release-x.y.z" 
   - prosjektet pusher release til Nexus
   - git tags/commits pushes tilbake til begge repos
  
4. afrr-solver rulles ut til systest av fifty sine devops mekanismer 


### Tips

#### Hvordan kjøre afrr-solver på Statnett utvikler PC

For å kjøre afrr-solver på lokal PC anbefales å benytte virtuelle python miljø. Det er også nødvendig å benytte statnett sitt pypi repo i Artifactory, fordi pip ikke uten videre kan nå ut pga Statnetts ssl proxy.
Dette er enkelt om du bruker makefilen i prosjektet.

Oppskrift for å sjekke ut koden, sette opp virtuelt miljø, installere 3-parts libs, og starte afrr-solver: 

```sh
# Klon prosjektet
git clone ssh://git@bitbucket.larm.statnett.no:7999/aof/afrr.git

# Opprett et virtuel python miljø og installer 3-parts avhengigheter
make devprep 

# aktiver virtuelt miljø
source ~/.virtualenvs/afrr/bin/activate

# Kjør afrr-solver 
python afrr-solver-starter.py
```

- For å teste kall med curl mot tjenesten kan du bruke hjelescriptet .bash/solve.sh
- Om du ønsker å kun teste markestsolver.py så er det best å bare lage sin egen wrapper som importerer pakken og kaller solve direkte
 

#### Hvordan bygge og publisere release

"make release" automatiserer følgende
 - Bygger zip fil
 - Tagger git repository med ny release (prompter for bekreftelse/overstyring av relase nummer)
 - Laster opp zip fil til Nexus

```sh
 # Se hvilke releaser som er gjort til nå
 git tag | grep release
 # Bygg og publiser release
 make release
```
 
#### Hvordan deploye en release slik det gjøres i produksjon
Eksempel under for versjon 0.0.1

```sh
# Last ned artefakt fra Nexus
wget http://repository.larm.statnett.no:8081/repository/mms-releases/fifty/optimeering/afrr-solver/0.0.1/afrr-solver-0.0.1.zip
 
# pakk ut
unzip afrr-solver-0.0.1.zip
cd afrr-solver-0.0.1
 
# opprett et virtuelt python miljø for afrr-solver og aktiver det
virtualenv ~/.virtualenvs/afrr
source ~/.virtualenvs/afrr/bin/activate
 
# installer 3-parts pakker, inklusiv fico xpress
pip install wheelhouse/*whl
 
# installer afrr_solver pakken
pip install afrr_solver*whl
 
# xpress community license er installert by default
# om du ønsker å kjøre mot floating lisens server gjør følgende :
# 1. fjerne community lisens
rm ~/.virtualenvs/afrr/lib/python3.6/site-packages/xpress/license/community-xpauth.xpr
# 2. legg inn lisens fil som peker på statnett lisensserver
echo "use_server server=h1-a-ficoat1.statnett.no" > ~/.virtualenvs/afrr/lib/python3.6/site-packages/xpress/license/xpauth.xpr
 
# start afrr-solver tjenesten med inkludert script som følger samme struktur som larm.sh scriptene
./afrr-solver.sh start
 
# Log kan inspiseres med
tail -f afrr-solver.log
 
# Test heartbeat for tjenesten
curl http://localhost:6000/heartbeat
 
# Test om lisens fungerer (gjør et "nop" kall mot fico)
curl http://localhost:6000/checklicense
``
