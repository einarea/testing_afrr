import pytest
from .data_objects import *
from .marketsolver import ModelClearing
from .database_initializer import Database
from .basemodel import Model, add_new_node_callback
from .model_classes import BidSolution
from .afrr_errors import XpressError
import time
import threading
from pathlib import Path
import json
import math

unittests_loc = Path(__file__).parents[1]/'regression_tests'

def openjson(filename):
    with (unittests_loc/filename).open() as f:
            data = json.load(f)
    return data


class TestJSONInstances:
    def test_input(self):
        req = openjson('input.json')
        model = ModelClearing(json_data=req)
        model.initialize()
        assert all([model.db.initialized, model.optimality_gap == 0.1, model.random_seed == 17,
                    model.gap_interval == 30, model.gap_increment == 0.2, model.max_time == 300])


class TestJsonInitialization:

    def test_general_input_error(self):
        data = {}
        with pytest.raises(InputError):
            Database(data)

    def test_general_input_error2(self):
        data = None
        model = ModelClearing(json_data=data)
        with pytest.raises(InputError):
            model.initialize()

    def test_general_input_error3(self):
        data = 4
        model = ModelClearing(json_data=data)
        with pytest.raises(InputError):
            model.initialize()

    def test_init_time_holder(self):

        data = {
            "id": "a7dfea74-a647-4a22-87a0-915767053ab4",
            "config": {
                "numberOfHours": 24,
            }
        }
        db = Database()
        db.data = data
        time_holder = db.init_time()
        hours = [i for i in range(1, 25)]
        hour_range = time_holder.hour_range(1)
        assert time_holder.hours == hours and len(hour_range) == 24

    def test_init_direction_holder(self):
        data = {"directions": [
            {
                  "id": 1,
                  "name": "UP"
                },
            {
                  "id": 2,
                  "name": "DOWN"
                }
              ]
        }
        db = Database()
        db.data = data
        direction_holder = db.init_direction()
        other = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        diff_other = DirectionHolder(up=2, down=3, up_code=UP, down_code=DOWN)
        assert direction_holder == other and direction_holder != diff_other

    def test_error_direction_holder(self):
        data = {"directions": [
            {
                "id": 1,
                "name": "UP"
            },
            {
                "id": 2,
                "name": "HIGHWAY_TO_HELL"
            }
        ]
        }
        db = Database()
        db.data = data
        with pytest.raises(InputError):
            db.init_direction()



    def test_init_region(self):
        db = Database()
        data = {"config": {
            "numberOfBiddingZones": 12,
            "penaltyRelaxDemandBiddingZone": 9999,
            "penaltyRelaxProdMinBiddingZone": 9999,
        }}
        db.data = data

        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        db.init_regions(time_holder, direction_holder)
        correct_nreg = len(db.regions) == 12
        correct_id = True
        i = 1
        for region in db.regions:
            if region.id != i:
                correct_id = False
            i += 1
        correct_dem_penalty = all([region.demand_penalty == 9999 for region in db.regions])
        correct_prod_min_penalty = all([region.prod_min_penalty == 9999 for region in db.regions])
        assert all([correct_id, correct_dem_penalty, correct_nreg, correct_prod_min_penalty])

    def test_area_prod_min_prod_max(self):
        time_holder = TimeHolder(3)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        area = MacroArea(1, time_holder, direction_holder)
        db = Database()
        db.macro_areas = DataVector([area])
        db.data = {"macroAreaDemands": [
            {
                "hour": 1,
                "macroArea": 1,
                "macroAreaCode": "A1",
                "direction": 1,
                "directionCode": "UP",
                "prodMin": 10,
                "prodMax": 20
            },
            {
                "hour": 2,
                "macroArea": 1,
                "macroAreaCode": "A1",
                "direction": 1,
                "directionCode": "UP",
                "prodMin": 20,
                "prodMax": 30
            },
            {
                "hour": 1,
                "macroArea": 1,
                "macroAreaCode": "A1",
                "direction": 2,
                "directionCode": "DOWN",
                "prodMin": 30,
                "prodMax": 40
            },
            {
                "hour": 2,
                "macroArea": 1,
                "macroAreaCode": "A1",
                "direction": 2,
                "directionCode": "DOWN",
                "prodMin": 40,
                "prodMax": 50
            },
            {
                "hour": 3,
                "macroArea": 1,
                "macroAreaCode": "A1",
                "direction": 2,
                "directionCode": "DOWN",
                "prodMin": 'a',
                "prodMax": 'a'
            }
        ]
        }
        db.init_area_prod_constraints()

        assert all([area.prod_min[1].up == 10, area.prod_min[2].up == 20, area.prod_min[1].down == 30,
                    area.prod_min[2].down == 40, area.prod_max[1].up == 20, area.prod_max[2].up == 30,
                    area.prod_max[1].down == 40, area.prod_max[2].down == 50, area.prod_min[3].down == 0,
                    area.prod_max[3].up is None])

    def test_no_prod_min_or_max(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region(1, time_holder, direction_holder)
        db = Database()
        db.regions = DataVector([reg1])

        data = {
            "biddingZoneDemands": [
                {
                    "hour": 1,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 1,
                    "directionCode": "UP",
                    "demand": 10,
                    "prodMin": None,
                    "prodMax": 'a'
                },
                {
                    "hour": 2,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 1,
                    "directionCode": "UP",
                    "demand": 20,
                    "prodMin": None,
                    "prodMax": 'a'

                },
                {
                    "hour": 1,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 2,
                    "directionCode": "DOWN",
                    "demand": 10,
                    "prodMin": 'a',
                    "prodMax": None
                },
                {
                    "hour": 2,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 2,
                    "directionCode": "DOWN",
                    "demand": 20,
                    "prodMin": None,
                    "prodMax": 'a'

                }
            ]}

        db.data = data
        db.init_demand()
        correct_max = reg1.prod_max[1].up is None and reg1.prod_max[1].down is None
        correct_min = reg1.prod_min[1].up == 0 and reg1.prod_min[1].down == 0
        assert all([correct_max, correct_min])

    def test_init_region_demand(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region(1, time_holder, direction_holder)
        reg2 = Region(2, time_holder, direction_holder)
        reg3 = Region(3, time_holder, direction_holder)
        db = Database()
        db.regions = DataVector([reg1, reg2, reg3])
        data = {
            "biddingZoneDemands": [
                {
                    "hour": 1,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 1,
                    "directionCode": "UP",
                    "demand": 10,
                    "prodMin": 10,
                    "prodMax": 1000
                },
                {
                    "hour": 2,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 1,
                    "directionCode": "UP",
                    "demand": 20,
                    "prodMin": 5,
                    "prodMax": 1001
                },
                {
                    "hour": 1,
                    "biddingZone": 3,
                    "biddingZoneCode": "DK2",
                    "direction": 1,
                    "directionCode": "UP",
                    "demand": 15,
                    "prodMin": 0,
                    "prodMax": 1002
                },
                {
                    "hour": 2,
                    "biddingZone": 3,
                    "biddingZoneCode": "DK2",
                    "direction": 1,
                    "directionCode": "UP",
                    "demand": 18,
                    "prodMin": 17,
                    "prodMax": 1003
                },
                {
                    "hour": 1,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 2,
                    "directionCode": "DOWN",
                    "demand": 2,
                    "prodMin": 0,
                    "prodMax": 1004
                },
                {
                    "hour": 2,
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "direction": 2,
                    "directionCode": "DOWN",
                    "demand": 4,
                    "prodMin": 2,
                    "prodMax": 1005
                },
                {
                    "hour": 1,
                    "biddingZone": 3,
                    "biddingZoneCode": "DK2",
                    "direction": 2,
                    "directionCode": "DOWN",
                    "demand": 100,
                    "prodMin": 4,
                    "prodMax": 1006
                },
                {
                    "hour": 2,
                    "biddingZone": 3,
                    "biddingZoneCode": "DK2",
                    "direction": 2,
                    "directionCode": "DOWN",
                    "demand": 22.5,
                    "prodMin": 6,
                    "prodMax": 1007
                }
            ]}
        db.data = data
        db.init_demand()
        correct_up = all([reg1.demand[1].up == 10, reg1.demand[2].up == 20, reg3.demand[1].up == 15,
                          reg3.demand[2].up == 18, reg2.demand[1].up == 0,  reg2.demand[2].up == 0])
        correct_down = all([reg1.demand[1].down == 2, reg1.demand[2].down == 4, reg3.demand[1].down == 100,
                            reg3.demand[2].down == 22.5, reg2.demand[1].down == 0,  reg2.demand[2].down == 0])
        correct_prod_min_up = all([reg1.prod_min[1].up == 10, reg1.prod_min[2].up == 5, reg3.prod_min[1].up == 0,
                          reg3.prod_min[2].up == 17, reg2.prod_min[1].up == 0, reg2.prod_min[2].down == 0])
        correct_prod_min_down = all([reg1.prod_min[1].down == 0, reg1.prod_min[2].down == 2, reg3.prod_min[1].down == 4,
                                   reg3.prod_min[2].down == 6, reg2.prod_min[1].up == 0, reg2.prod_min[2].down == 0])
        correct_prod_max_up = all([reg1.prod_max[1].up == 1000, reg1.prod_max[2].up == 1001, reg3.prod_max[1].up == 1002,
                          reg3.prod_max[2].up == 1003, reg2.prod_max[1].up is None, reg2.prod_max[2].up is None])

        correct_prod_max_down = all(
            [reg1.prod_max[1].down == 1004, reg1.prod_max[2].down == 1005, reg3.prod_max[1].down == 1006,
             reg3.prod_max[2].down == 1007, reg2.prod_max[1].down is None, reg2.prod_max[2].down is None])

        assert all([correct_up, correct_down, correct_prod_min_up, correct_prod_min_down, correct_prod_max_up,
                    correct_prod_max_down])

    def test_init_macro_areas(self):
        db = Database()
        data = {"config": {
            "numberOfMacroAreas": 2,
            "penaltyRelaxProdMinMacroArea": 9999
        },
            "biddingZonesMacroAreas": [
                {
                    "biddingZone": 1,
                    "biddingZoneCode": "DK2",
                    "macroArea": 2,
                    "macroAreaCode": "A2"
                },
                {
                    "biddingZone": 3,
                    "biddingZoneCode": "NO1",
                    "macroArea": 1,
                    "macroAreaCode": "A1"
                },
                {
                    "biddingZone": 5,
                    "biddingZoneCode": "NO2",
                    "macroArea": 1,
                    "macroAreaCode": "A1"
                }]
        }
        db.data = data
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region(1, time_holder, direction_holder)
        reg3 = Region(3, time_holder, direction_holder)
        reg5 = Region(5, time_holder, direction_holder)
        db.regions = DataVector([reg1, reg3, reg5])
        db.init_macro_areas(time_holder, direction_holder)
        correct_len = len(db.macro_areas) == 2
        correct_id = True
        i = 1
        for area in db.macro_areas:
            if area.id != i:
                correct_id = False
            i += 1

        correct_prod_min_penalty = all([area.prod_min_penalty == 9999 for area in db.macro_areas])
        m_1 = db.macro_areas.filter_object(lambda area: area.id == 1)
        m_2 = db.macro_areas.filter_object(lambda area: area.id == 2)
        correct_reg1 = reg1 in m_2.regions and len(m_2.regions) == 1
        correct_reg2 = reg3 in m_1.regions and reg5 in m_1.regions and len(m_1.regions) == 2

        assert all([correct_len, correct_id, correct_prod_min_penalty, correct_reg1, correct_reg2])

    def test_init_nordic_area(self):
        data = {
            "id": "id_string"
        }
        db = Database()
        db.data = data
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region(1, time_holder, direction_holder)
        reg3 = Region(3, time_holder, direction_holder)
        reg5 = Region(5, time_holder, direction_holder)
        db.regions = DataVector([reg1, reg3, reg5])
        m_1 = MacroArea(1, time_holder, direction_holder)
        m_1.regions = reg1
        m_2 = MacroArea(2, time_holder, direction_holder)
        m_2.regions = DataVector([reg3, reg5])
        db.macro_areas = DataVector([m_1, m_2])
        nordic_area = db.init_nordic_area(time_holder, direction_holder)
        correct_id = nordic_area.instance_id == "id_string"
        correct_regions = all([reg in nordic_area.regions for reg in db.regions])
        correct_areas = all([area in nordic_area.macro_areas for area in db.macro_areas])
        assert all([correct_id, correct_regions, correct_areas])

    def test_init_connections(self):
        data =  {"transferCapacities": [
    {
      "hour": 1,
      "biddingZoneIn": 2,
      "biddingZoneInCode": "DK2",
      "biddingZoneOut": 1,
      "biddingZoneOutCode": "SE4",
      "capacity": 10,
      "cost": 5
    },
    {
      "hour": 2,
      "biddingZoneIn": 1,
      "biddingZoneInCode": "DK2",
      "biddingZoneOut": 2,
      "biddingZoneOutCode": "SE4",
      "capacity": 20,
      "cost": 10
    },
    {
        "hour": 1,
        "biddingZoneIn": 3,
        "biddingZoneInCode": "DK2",
        "biddingZoneOut": 2,
        "biddingZoneOutCode": "SE4",
        "capacity": 30,
        "cost": 15
    },
    {
        "hour": 2,
        "biddingZoneIn": 2,
        "biddingZoneInCode": "DK2",
        "biddingZoneOut": 3,
        "biddingZoneOutCode": "SE4",
        "capacity": 40,
        "cost": 20
    }
]}
        db = Database()
        db.data = data
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region(1, time_holder, direction_holder)
        reg2 = Region(2, time_holder, direction_holder)
        reg3 = Region(3, time_holder, direction_holder)
        db.regions = DataVector([reg1, reg2, reg3])
        db.init_connections()
        con1 = reg1.OutEdges[0]
        con2 = reg2.OutEdges[0]
        con3 = reg2.OutEdges[1]
        con4 = reg3.OutEdges[0]

        cor_1 = all([len(reg1.OutEdges) == 1, con1[1].cost == 5, con1[1].capacity == 10, con1.from_area == reg1,
                     con1.to_area == reg2, con1 in reg2.InEdges, len(reg2.InEdges) == 2])
        cor_2 = all([len(reg2.OutEdges) == 2, con2[2].cost == 10, con2[2].capacity == 20, con2.from_area == reg2,
                     con2.to_area == reg1, con2 in reg1.InEdges and len(reg1.InEdges) == 1])
        cor_3 = all([con3[1].cost == 15, con3[1].capacity == 30, con3.from_area == reg2, con3.to_area == reg3,
                     con3 in reg3.InEdges and len(reg3.InEdges) == 1])
        cor_4 = all([len(reg3.OutEdges) == 1, con4[2].cost == 20, con4[2].capacity == 40, con4.from_area == reg3,
                     con4[1].capacity == 0, con4.to_area == reg2, con4 in reg2.InEdges])
        assert all([cor_1, cor_2, cor_3, cor_4])

    def test_init_bids(self):
        data={"config": {
            "quantityFactor": 5
        },"bids": [
            {
                "id": 1,
                "hourFrom": 1,
                "hourTo": 2,
                "biddingZone": 1,
                "biddingZoneCode": "NO5",
                "direction": 2,
                "directionCode": "DOWN",
                "minVolume": 10,
                "block": False,
                "volume": 20,
                "price": 3,
                "bspCode": "D_2018_12_30_01_38_ECO",
                "bidNumber": "1"
            },
            {
                "id": 2,
                "hourFrom": 2,
                "hourTo": 3,
                "biddingZone": 1,
                "biddingZoneCode": "NO5",
                "direction": 2,
                "directionCode": "DOWN",
                "minVolume": 5,
                "block": False,
                "volume": 30,
                "price": 5,
                "bspCode": "D_2018_12_30_01_38_ECO",
                "bidNumber": "1"
            },
            {
                "id": 3,
                "hourFrom": 1,
                "hourTo": 2,
                "biddingZone": 2,
                "biddingZoneCode": "NO5",
                "direction": 1,
                "directionCode": "UP",
                "minVolume": 4,
                "block": False,
                "volume": 40,
                "price": 7,
                "bspCode": "D_2018_12_30_01_38_ECO",
                "bidNumber": "1"
            },
            {
                "id": 4,
                "hourFrom": 2,
                "hourTo": 3,
                "biddingZone": 2,
                "biddingZoneCode": "NO5",
                "direction": 1,
                "directionCode": "UP",
                "minVolume": None,
                "block": False,
                "volume": 50,
                "price": 9,
                "bspCode": "D_2018_12_30_01_38_ECO",
                "bidNumber": "1"
            }
        ]
        }
        db = Database()
        db.data = data
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region(1, time_holder, direction_holder)
        reg2 = Region(2, time_holder, direction_holder)
        db.regions = DataVector([reg1, reg2])
        db.init_bids()
        down_bids = reg1.down_bids
        up_bids = reg2.up_bids
        bid1= down_bids.get_bid(1)
        bid2 = down_bids.get_bid(2)

        bid3 = up_bids.get_bid(3)
        bid4 = up_bids.get_bid(4)
        cor1 = all([bid1.hour_from == 1, bid1.hour_to == 2, bid1.quantity_factor == 5, bid1.min_vol == 10,
                    bid1.volume==20, bid1.price == 3, bid1.direction == direction_holder.down])
        cor2 = all([bid2.hour_from == 2, bid2.hour_to == 3, bid2.quantity_factor == 5, bid2.min_vol == 5,
                    bid2.volume == 30, bid2.price == 5, bid2.direction == direction_holder.down])
        cor3 = all([bid3.hour_from == 1, bid3.hour_to == 2, bid3.quantity_factor == 5, bid3.min_vol == 4,
                    bid3.volume == 40, bid3.price == 7, bid3.direction == direction_holder.up])
        cor4 = all([bid4.hour_from == 2, bid4.hour_to == 3, bid4.quantity_factor == 5, bid4.min_vol == 50,
                    bid4.volume == 50, bid4.price == 9, bid4.direction == direction_holder.up])
        cor_length = all([len(reg1.all_bids)==2, len(reg2.all_bids) == 2])
        cor_length2 = all([len(reg1.all_bids[1])==1, len(reg1.all_bids[1])==1, len(reg1.all_bids[1])==1,
        len(reg1.all_bids[1]) == 1])
        cor_length3 = len(up_bids)==2 and len(down_bids)==2
        assert all([cor1, cor2, cor3, cor4, cor_length, cor_length2, cor_length3])

    def test_non_consecutive_bids(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        Bid(_id=1, direction=1, region=reg1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        Bid(_id=2, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        Bid(_id=4, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        regions = DataVector([reg1])
        db = Database()
        db.nordic_area = NordicArea(time_holder, direction_holder, regions, [])
        with pytest.raises(InputError):
            db.link_bids()

    def test_control_bids(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        Bid(_id=1, direction=1, region=reg1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        Bid(_id=1, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        db = Database()
        db.nordic_area = NordicArea(time_holder, direction_holder, [reg1], [])
        with pytest.raises(InputError):
            db.control_bid_ids()

    def test_link_bids(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        bid3 = Bid(_id=3, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        db = Database()
        db.data = {
        "linkedBids": [
            [
                1,
                2
            ],
            [
                2,
                3
            ],
            [
                1,
                2,
                3
            ]
        ]
        }
        regions = DataVector([reg1])
        db.nordic_area = NordicArea(time_holder, direction_holder, regions, [])
        db.link_bids()
        link1 = DataVector([bid1, bid2])
        link2 = DataVector([bid2, bid3])
        link3 = DataVector([bid1, bid2, bid3])
        links = DataVector([link1, link2, link3])
        correct_link = True
        i = 0
        for link in links:
            if not all([bid in link for bid in db.nordic_area.linked_bids[i]]) or \
                    not all([len(link) == len(db.nordic_area.linked_bids[i])]):
                correct_link = False
            i += 1

        assert correct_link

    def test_exclusive_bids(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        bid3 = Bid(_id=3, direction=2, region=reg1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        db = Database()
        db.data = {

        "exclusiveBids": [
            [
                1,
                2],
            [
                2,
                3
            ]
        ]}
        regions = DataVector([reg1])
        db.nordic_area = NordicArea(time_holder, direction_holder, regions, [])
        db.nordic_area.add_linked_bids([bid1, bid2])
        db.exclusive_bids()
        ex1 = DataVector([bid1, bid2])
        ex2 = DataVector([bid2, bid3])
        exes = DataVector([ex1, ex2])
        correct_ex = True
        i = 0
        for ex in exes:
            if not all([bid in ex for bid in db.nordic_area.exclusive_bids[i]]) or \
                    not all([len(ex) == len(db.nordic_area.exclusive_bids[i])]):
                correct_ex = False
            i += 1
        correct_coeff = all([bid1.exclusive_bid_coefficient == 0.5, bid2.exclusive_bid_coefficient == 0.5,
                             bid3.exclusive_bid_coefficient == 1])
        assert correct_ex and correct_coeff

    def test_init_optimization_parameters(self):
        data = {"config": {
                "maxProcessingTime": 300,
                "optimalityGap": 0.1,
                "gapIncrement": 1e-8,
                "gapInterval": 30,
                "randomSeed": 17,
                }}
        db = Database()
        db.data = data
        db.init_optimization_parameters()
        assert all([db.max_time==300, db.optimality_gap==0.1, db.gap_increment==1e-8, db.gap_interval==30,
                    db.random_seed==17])

    def test_error_random_seed(self):
        data = {"config": {
            "maxProcessingTime": 300,
            "optimalityGap": 0.1,
            "gapIncrement": 1e-8,
            "gapInterval": 30,
            "randomSeed": 2147483648,
        }}
        db = Database()
        db.data = data
        with pytest.raises(InputError):
            db.init_optimization_parameters()

    def test_error_gap(self):
        data = {"config": {
            "maxProcessingTime": 300,
            "optimalityGap": 'a',
            "gapIncrement": 1e-8,
            "gapInterval": 30,
            "randomSeed": 2147483648,
        }}
        db = Database()
        db.data = data
        with pytest.raises(InputError):
            db.init_optimization_parameters()

    def test_error_max_time(self):
        data = {"config": {
            "maxProcessingTime": 299.6,
            "optimalityGap": 0.1,
            "gapIncrement": 1e-8,
            "gapInterval": 30,
            "randomSeed": 2147483648,
        }}
        db = Database()
        db.data = data
        with pytest.raises(InputError):
            db.init_optimization_parameters()


"""Purpose: Test report based on solution information"""


class TestBidSolution:
    def test_init(self):
        time_holder = TimeHolder(2)
        sol = BidSolution(time_holder, 'run_id')
        assert sol.status == BidSolution.NO_SOL

    def test_report(self):
        instance_id = 'run1'
        time_holder = TimeHolder(2)
        sol = BidSolution(time_holder, instance_id)
        report = sol.get_report()
        cor_status1 = report[STATUS] == BidSolution.TimeoutStatus and report[LOG][0] == BidSolution.messages[TIMEOUT_NOSOL] % instance_id
        sol.status = BidSolution.MIP_INF
        report = sol.get_report()
        cor_status2 = report[STATUS] == BidSolution.FailStatus and report[LOG][0] == BidSolution.messages[INFEASIBLE] % instance_id
        sol.status = BidSolution.SOL_FOUND
        sol.set_relaxed(True)
        sol.set_sol_type_status()
        report = sol.get_report()
        cor_status3 = all([report[STATUS] == BidSolution.OptStatus+'_'+BidSolution.RelaxStatus,
                           report[LOG][0] == BidSolution.messages[CLEARED] % instance_id,
                           report[LOG][1] == BidSolution.messages[RELAXED], len(report[LOG]) == 2])
        sol.gap_set(lb=1, ub=1.1)
        sol.set_sol_type_status()
        report = sol.get_report()
        cor_status4 = all([report[STATUS] == BidSolution.SubStatus+'_'+BidSolution.RelaxStatus,
                           report[LOG][0] == BidSolution.messages[CLEARED] % instance_id,
                           report[LOG][1] == BidSolution.messages[TIMEOUT],
                           report[LOG][2] == BidSolution.messages[RELAXED], len(report[LOG]) == 3])

        sol.gap_set(lb=1, ub=1)
        sol.set_sol_type_status()
        sol.set_relaxed(False)
        report = sol.get_report()
        cor_status5 = all([report[STATUS] == BidSolution.OptStatus,
                           report[LOG][0] == BidSolution.messages[CLEARED] % instance_id,
                           report[LOG][1] == BidSolution.messages[OPTIMAL],
                           len(report[LOG]) == 2])

        sol.gap_set(lb=1, ub=1.1)
        sol.set_sol_type_status()
        report = sol.get_report()
        cor_status6 = all([report[STATUS] == BidSolution.SubStatus,
                           report[LOG][0] == BidSolution.messages[CLEARED] % instance_id,
                           report[LOG][1] == BidSolution.messages[TIMEOUT],
                           len(report[LOG]) == 2])
        assert all([cor_status1, cor_status2, cor_status3, cor_status4, cor_status5, cor_status6])


"""Purpose: Test region object """


class TestRegionObject:
    def test_init(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region(_id=1, time_holder=time_holder, direction_holder=direction_holder)
        hours = [1, 2]
        assert reg.id == 1 and reg.hours == hours and reg.direction_holder == direction_holder

    def test__init_error(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        with pytest.raises(InitializationError):
            Region(_id=1, time_holder=time_holder, direction_holder=direction_holder, prod_min_penalty='a')


"""Purpose: Test region object """


class TestMacroAreaObject:
    def test_init(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        area = MacroArea(_id=1, time_holder=time_holder, direction_holder=direction_holder, prod_penalty=10)
        assert area.id == 1 and area.time_holder == time_holder and area.direction_holder == direction_holder

    def test__init_error(self):
        with pytest.raises(InputError):
            MacroArea(_id=1, time_holder=1, direction_holder=1, prod_penalty=10)


class TestDirectionHolder:
    def test_init(self):
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        assert all([direction_holder.up == 1, direction_holder.down == 2,
                    direction_holder.up_code == UP, direction_holder.down_code == DOWN])

    def test_error_init(self):
        with pytest.raises(InitializationError):
            DirectionHolder(up='a', down=2, up_code=UP, down_code=DOWN)

    def test_get_direction(self):
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        assert direction_holder.get_code(1) == UP and direction_holder.get_code(2) == DOWN

    def test_error_direction_code(self):
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        with pytest.raises(ImplementationError):
            direction_holder.get_code(3)


class TestTimeHolder:
    def test_init(self):
        time_holder = TimeHolder(2)
        time_holder2 = TimeHolder(2)
        time_holder3 = TimeHolder(3)
        assert time_holder == time_holder2 and time_holder != time_holder3

    def test_init_error(self):
        with pytest.raises(InitializationError):
            TimeHolder('a')

    def test_class_init(self):
        hour_range = HourRange([1])
        time_holder1 = hour_range.get_hour_range(TimeHolder, 2)[1]
        time_holder2 = TimeHolder(2)
        time_holder3 = hour_range.get_hour_range(TimeHolder)[1]
        time_holder4 = TimeHolder()
        assert time_holder2 == time_holder1 and time_holder3 == time_holder4

    def test_hour_range(self):
        hour_range = HourRange([1, 2, 3])
        holder_comp = [1, 1, 1]
        holder = hour_range.get_hour_range(1)
        assert holder_comp == holder.Hours

    def test_reference_type_hour_range(self):
        hour_range = HourRange([1, 2])
        a = {'dem': 0}
        hour_range.get_hour_range(a)
        hour_range[2]['dem'] = 2
        assert all([hour_range[1]['dem'] == 0, hour_range[2]['dem'] == 2])

    def test_reference_type_hour_range2(self):
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region(_id=1, time_holder=time_holder, direction_holder=direction_holder)
        hour_range = HourRange([1, 2])
        hour_range.get_hour_range(reg)
        reg1 = hour_range[1]
        reg1.set_demand(1, 10, 1)
        assert reg1.demand[1].up == 10 and reg.demand[1].up == 0


"""Purpose: Test region object """

class TestNordicAreaObject:
    def test_init(self):
        pass
    def test__init_error(self):
        pass

"""Purpose: Test connection object """


class TestConnectionObject:
    def test_error_capacity(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        reg2 = Region('NO2', time_holder, direction_holder)

        with pytest.raises(InitializationError):
            Connection(reg1, reg2, 'a')

    def test_error_from_area(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        with pytest.raises(InitializationError):
            Connection(None, reg1, 10)

    def test_error_to_area(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        with pytest.raises(InitializationError):
            Connection(reg1, None, 10)

    def test_init(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg1 = Region('NO1', time_holder, direction_holder)
        reg2 = Region('NO2', time_holder, direction_holder)
        connection = Connection(reg1, reg2, 10)

        assert (connection.from_area == reg1 and connection.to_area == reg2 and connection.capacity == 10
                and type(connection[1]) is Flow)


"""Purpose: Test bid object """


class TestBidObject:

    def test_hour_outside_time_range(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        hour_from = 'a'
        with pytest.raises(InitializationError):
            Bid(_id=1, direction=1, region=reg, hour_from=hour_from, hour_to=2, volume=10,
                   price=10, min_vol=2)

    def test_init(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        _id = 1
        hour_from = 1
        hour_to = 2
        volume = 100
        price = 10
        min_vol = 2
        bid = Bid(_id=1, direction=1, region=reg, hour_from=hour_from, hour_to=hour_to, volume=volume,
                  price=price, min_vol=min_vol)
        assert all([bid.id == _id, bid.region == reg, bid.volume == bid.volume,
                    bid.price == price, bid.min_vol == 2])

    def test_bid_type(self):
        time_holder = TimeHolder(1)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        region = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=region, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=region, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        assert bid1.type == 1 and bid2.type == 2 and bid1.direction_code == UP and bid2.direction_code == DOWN

    def test_error_region(self):
        _id = 1
        region = 'region'
        with pytest.raises(InitializationError):
            Bid(_id=_id, direction=1, region=region, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)


class TestNordicClearing:
    def test_flow_nordic_clearing_1(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_a = Region(1, time_holder, direction_holder, demand_penalty=9999)
        reg_b = Region(2, time_holder, direction_holder, demand_penalty=9999)
        reg_a.set_demand(hour=1, value=10, direction=up)
        reg_b.set_demand(hour=1, value=10, direction=up)
        reg_a.set_demand(hour=1, value=5, direction=down)
        reg_b.set_demand(hour=1, value=20, direction=down)

        con_a_b = Connection(from_area=reg_a, to_area=reg_b, capacity=50)
        con_a_b.set_cost(hour=1, cost=5)
        con_b_a = Connection(from_area=reg_b, to_area=reg_a, capacity=50)
        con_b_a.set_cost(hour=1, cost=5)

        bid1 = Bid(_id=1, direction=up, region=reg_a, hour_from=1, hour_to=2, volume=10, price=5)
        bid2 = Bid(_id=2, direction=up, region=reg_a, hour_from=1, hour_to=2, volume=15, price=6)
        bid3 = Bid(_id=3, direction=down, region=reg_a, hour_from=1, hour_to=2, volume=5, price=16)
        bid4 = Bid(_id=4, direction=down, region=reg_a, hour_from=1, hour_to=2, volume=10, price=20)
        bid5 = Bid(_id=5, direction=up, region=reg_b, hour_from=1, hour_to=2, volume=10, price=25)
        bid6 = Bid(_id=6, direction=up, region=reg_b, hour_from=1, hour_to=2, volume=15, price=36)
        bid7 = Bid(_id=7, direction=down, region=reg_b, hour_from=1, hour_to=2, volume=10, price=46)

        selected_bids = DataVector([bid1, bid2, bid3, bid4, bid7])
        unselected_bids = [bid5, bid6]

        all_regions = [reg_a, reg_b]
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_areas=[])

        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up()
        solution = model.solve()
        correct_bids = all([bid in nordic_area.accepted_bids for bid in selected_bids])
        correct_n_selected_bids = len(nordic_area.accepted_bids) == len(selected_bids)
        correct_unselected_bids = all([bid not in nordic_area.accepted_bids for bid in unselected_bids])
        correct_objective = math.isclose(nordic_area.total_cost,
                                         solution.objective_value)

        correct_flow = con_a_b[1].volume_sol == 10 and con_b_a[1].volume_sol == 10
        correct_direction_flow = all([con_a_b[1].down.solution_val == 10, con_a_b[1].up.solution_val == 10,
                                      con_b_a[1].up.solution_val == 0, con_b_a[1].down.solution_val == 0])

        correct_volume = all([math.isclose(bid.volume_sol, bid.volume) for bid in selected_bids])
        correct_unselected_volume = all([bid.volume_sol == 0 for bid in unselected_bids])
        assert all([correct_bids, correct_n_selected_bids, correct_volume, correct_flow, correct_objective,
                    correct_unselected_bids, correct_unselected_volume, correct_direction_flow])

    def test_flow_nordic_clearing_2(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_a = Region(1, time_holder, direction_holder, demand_penalty=9999)
        reg_b = Region(2, time_holder, direction_holder, demand_penalty=9999)
        reg_a.set_demand(hour=1, value=15, direction=up)
        reg_b.set_demand(hour=1, value=10, direction=up)
        reg_a.set_demand(hour=1, value=5, direction=down)
        reg_b.set_demand(hour=1, value=20, direction=down)

        con_a_b = Connection(from_area=reg_a, to_area=reg_b, capacity=50)
        con_a_b.set_cost(hour=1, cost=5)
        con_b_a = Connection(from_area=reg_b, to_area=reg_a, capacity=50)
        con_b_a.set_cost(hour=1, cost=5)

        bid1 = Bid(_id=1, direction=up, region=reg_a, hour_from=1, hour_to=2, volume=10, price=55)
        bid2 = Bid(_id=2, direction=up, region=reg_a, hour_from=1, hour_to=2, volume=15, price=60)
        bid3 = Bid(_id=3, direction=down, region=reg_a, hour_from=1, hour_to=2, volume=5, price=16)
        bid4 = Bid(_id=4, direction=down, region=reg_a, hour_from=1, hour_to=2, volume=10, price=20)
        bid5 = Bid(_id=5, direction=up, region=reg_b, hour_from=1, hour_to=2, volume=10, price=25)
        bid6 = Bid(_id=6, direction=up, region=reg_b, hour_from=1, hour_to=2, volume=15, price=36)
        bid7 = Bid(_id=7, direction=down, region=reg_b, hour_from=1, hour_to=2, volume=10, price=46)

        selected_bids = DataVector([bid3, bid4, bid5, bid6, bid7])
        unselected_bids = [bid1, bid2]

        all_regions = [reg_a, reg_b]
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_areas=[])

        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up()
        solution = model.solve()
        correct_bids = all([bid in nordic_area.accepted_bids for bid in selected_bids])
        correct_n_selected_bids = len(nordic_area.accepted_bids) == len(selected_bids)
        correct_unselected_bids = all([bid not in nordic_area.accepted_bids for bid in unselected_bids])
        correct_objective = math.isclose(nordic_area.total_cost,
                                         solution.objective_value)

        correct_flow = con_a_b[1].volume_sol == 0 and con_b_a[1].volume_sol == 15
        correct_direction_flow = all([con_a_b[1].down.solution_val == 10, con_a_b[1].up.solution_val == 0,
                                      con_b_a[1].up.solution_val == 15, con_b_a[1].down.solution_val == 0])

        correct_volume = all([math.isclose(bid.volume_sol, bid.volume) for bid in selected_bids])
        correct_unselected_volume = all([bid.volume_sol == 0 for bid in unselected_bids])
        assert all([correct_bids, correct_n_selected_bids, correct_volume, correct_flow, correct_objective,
                    correct_unselected_bids, correct_unselected_volume, correct_direction_flow])

    def test_demand_nordic_area(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2

        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)
        reg_2 = Region(2, time_holder, direction_holder)
        reg_2.set_demand(1, 7, up)
        reg_1.set_demand(1, 4, up)
        reg_2.set_demand(1, 2, down)
        reg_1.set_demand(1, 4, down)
        all_regions = [reg_1, reg_2]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = all_regions
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_area_1)
        assert nordic_area.demand[1].up() == 11 and nordic_area.demand[1].down() == 6

    def test_infeasible_problem(self):
        time_holder = TimeHolder(10)
        up = 1
        down = 2
        penalty = 9999
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        reg_2 = Region(2, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        Connection(reg_1, reg_2, 100, cost=10)
        Connection(reg_2, reg_1, 100, cost=10)
        all_regions = [reg_1, reg_2]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = all_regions
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_area_1)
        macro_area_1.set_prod_min(5, 1, up)
        macro_area_1.set_prod_max(20, 1, up)
        reg_2.set_prod_min(5, 1, down)
        reg_2.set_demand(1, 7, down)
        reg_1.set_demand(10, 4, up)
        reg_2.set_prod_max(10, 1, down)
        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up(soft=False)
        response = model.clearmarket()
        assert response[STATUS] == 'FAIL'

    def test_relaxed_solution(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2
        penalty = 9999
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        reg_2 = Region(2, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        Connection(reg_1, reg_2, 100, cost=10)
        Connection(reg_2, reg_1, 100, cost=10)
        all_regions = [reg_1, reg_2]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = all_regions
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_area_1)
        macro_area_1.set_prod_min(5, 1, up)
        macro_area_1.set_prod_min(10, 1, down)
        macro_area_1.set_prod_max(20, 1, up)
        reg_2.set_prod_min(5, 1, down)
        reg_1.set_prod_min(5, 1, up)
        reg_2.set_demand(1, 7, down)
        reg_1.set_demand(1, 4, up)
        reg_2.set_demand(1, 4, up)
        reg_2.set_prod_max(10, 1, down)
        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up()
        response = model.clearmarket()
        assert all([response[STATUS] == 'OPT_RELAX', reg_2.demand_penalty_var[1].down.solution_val == 7,
                    reg_1.demand_penalty_var[1].up.solution_val == 4, len(response[REGION_DEMAND_REL]) == 3,
                    len(response[MACRO_DEMAND_REL]) == 2])

    def test_relaxed_solution2(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2
        penalty = 9999
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        reg_2 = Region(2, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        Connection(reg_1, reg_2, 100, cost=10)
        Connection(reg_2, reg_1, 100, cost=10)
        all_regions = [reg_1, reg_2]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = all_regions
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_area_1)

        reg_1.set_prod_min(5, 1, up)
        reg_2.set_demand(1, 7, down)

        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up()
        response = model.clearmarket()
        _dict = response[REGION_DEMAND_REL]
        _reg_1 = {}
        _reg_2 = {}
        for _element in _dict:
            if _element[REGION] == 1:
                _reg_1 = _element
            else:
                _reg_2 = _element

        assert all([response[STATUS] == 'OPT_RELAX', reg_2.demand_penalty_var[1].down.solution_val == 7,
                    reg_2.demand_penalty_var[1].down.solution_val == 7, len(_reg_1) == 4,
                    PROD_MIN in _reg_1.keys(),
                    _reg_1[PROD_MIN] == 0,
                    DEMAND in _reg_2.keys(),
                    _reg_2[DEMAND] == 0,
                    len(response[MACRO_DEMAND_REL]) == 0])

    def test_multiple_region_single_hour(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2
        penalty = 9999
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        reg_2 = Region(2, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        Connection(reg_1, reg_2, 100, cost = 10)
        Connection(reg_2, reg_1, 100, cost=10)
        all_regions = [reg_1, reg_2]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = all_regions
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_area_1)
        bid1 = Bid(_id=1, direction=up, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=down, region=reg_2, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid3 = Bid(_id=3, direction=up, region=reg_1, hour_from=1, hour_to=2, volume=10, price=2, min_vol=2)
        bid4 = Bid(_id=4, direction=up, region=reg_1, hour_from=1, hour_to=2, volume=10, price=2, min_vol=2)
        linked_bids = [bid1, bid3]
        exclusive_bids = [bid1, bid3, bid4]
        nordic_area.add_linked_bids(linked_bids)
        nordic_area.add_exclusive_bids(exclusive_bids)
        macro_area_1.set_prod_min(5, 1, up)
        macro_area_1.set_prod_max(20, 1, up)
        macro_area_1.set_prod_max(20, 1, down)
        reg_2.set_prod_min(5, 1, down)
        reg_2.set_demand(1, 7, down)
        reg_1.set_demand(1, 4, up)
        reg_2.set_prod_max(10, 1, down)
        reg_2.set_prod_max(50, 1, up)
        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up(soft=False)
        solution = model.solve()
        assert solution.objective_value == 80 and bid4 in solution.accepted_bids and bid2 in solution.accepted_bids


"""Purpose: Test geographic structure and correct data for objects """


class TestObjectStructure:

    def test_nordic_demand(self):
        time_holder = TimeHolder(5)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)
        reg_2 = Region(2, time_holder, direction_holder)
        reg_1.set_demand(hour=1, value=10, direction=1)
        reg_1.set_demand(hour=1, value=5, direction=2)
        reg_1.set_demand(hour=2, value=5, direction=1)
        reg_2.set_demand(hour=1, value=15, direction=1)
        reg_2.set_demand(hour=1, value=11, direction=2)
        reg_2.set_demand(hour=2, value=11, direction=2)
        all_regions = [reg_1, reg_2]
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, [])
        assert all([nordic_area.demand[1].up() == 25, nordic_area.demand[1].down() == 16,
                    nordic_area.demand[2].up() == 5, nordic_area.demand[2].down() == 11])



    def test_geography(self):
        time_holder = TimeHolder(5)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)
        reg_2 = Region(2, time_holder, direction_holder)
        reg_3 = Region(3, time_holder, direction_holder)
        reg_4 = Region(4, time_holder, direction_holder)

        all_regions = [reg_1, reg_2, reg_3, reg_4]
        regions_1 = [reg_1, reg_2]
        regions_2 = [reg_1, reg_3]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = regions_1
        macro_area_2 = MacroArea(2, time_holder, direction_holder)
        macro_area_2.regions = regions_2
        macro_areas = [macro_area_1, macro_area_2]
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_areas)

        bid1 = Bid(_id=1, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=reg_2, hour_from=2, hour_to=5, volume=15, price=9, min_vol=4)
        bidset=BidSet(time_holder, [bid1, bid2])
        Connection()
        correct_regions1 = all(region in nordic_area.regions for region in all_regions)
        correct_regions2 = len(all_regions) == len(nordic_area.regions)
        correct_regions3 = all(region in macro_area_1.regions for region in regions_1)
        correct_regions4 = len(regions_1) == len(macro_area_1.regions)
        correct_macro1 = reg_2.macro_area == macro_area_1
        correct_macro2 = all([len(reg_1.macro_area) == 2, macro_area_1 in reg_1.macro_area,
                              macro_area_2 in reg_1.macro_area])

        assert all([correct_regions1, correct_regions2, correct_regions3, correct_regions4, correct_macro1, correct_macro2, False])


    def test_bidset(self):
        time_holder = TimeHolder(5)
        direction_holder = DirectionHolder(up=1, down = 2, up_code = UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)
        reg_2 = Region(2, time_holder, direction_holder)
        reg_3 = Region(3, time_holder, direction_holder)
        reg_4 = Region(4, time_holder, direction_holder)

        all_regions = [reg_1, reg_2, reg_3, reg_4]
        regions_1 = [reg_1, reg_2]
        regions_2 = [reg_3]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = regions_1
        macro_area_2 = MacroArea(2, time_holder, direction_holder)
        macro_area_2.regions = regions_2
        macro_areas = [macro_area_1, macro_area_2]
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_areas)

        bid1 = Bid(_id=1, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=reg_2, hour_from=2, hour_to=5, volume=15, price=9, min_vol=4)
        bid3 = Bid(_id=3, direction=1, region=reg_3, hour_from=2, hour_to=4, volume=20, price=8, min_vol=3)
        bid4 = Bid(_id=4, direction=2, region=reg_4, hour_from=5, hour_to=6, volume=30, price=7, min_vol=2)
        bid5 = Bid(_id=5, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=40, price=6, min_vol=5)
        bid6 = Bid(_id=6, direction=2, region=reg_2, hour_from=1, hour_to=3, volume=50, price=5, min_vol=10)

        all_bids = DataVector([bid1, bid2, bid3, bid4, bid5, bid6])
        up_bids = DataVector([bid1, bid3, bid5])
        down_bids = DataVector([bid2, bid4, bid6])
        correct_bids1 = all([bid in nordic_area.all_bids for bid in all_bids])
        correct_bids2 = len(all_bids) == len(nordic_area.all_bids) and type(nordic_area.all_bids) is BidSet
        correct_bids3 = all([bid in nordic_area.down_bids for bid in down_bids])
        correct_bids4 = len(down_bids) == len(nordic_area.down_bids) and type(nordic_area.down_bids) is BidSet
        correct_bids5 = all([bid in nordic_area.up_bids for bid in up_bids])
        correct_bids6 = len(up_bids) == len(nordic_area.up_bids)

        second_hour_bids = DataVector([bid2, bid3, bid6])
        correct_bids7 = all([bid in nordic_area.all_bids[2] for bid in second_hour_bids])
        correct_bids8 = len(nordic_area.all_bids[2]) == len(second_hour_bids)

        second_hour_down_bids = DataVector([bid2, bid6])
        correct_bids9 = all([bid in nordic_area.down_bids[2] for bid in second_hour_down_bids])
        correct_bids10 = len(nordic_area.down_bids[2]) == len(second_hour_down_bids)

        macro_1_bids = DataVector([bid1, bid5, bid2, bid6])
        correct_bids11 = all([bid in macro_area_1.all_bids for bid in macro_1_bids])
        correct_bids12 = len(macro_area_1.all_bids) == len(macro_1_bids) and type(macro_area_1.all_bids) is BidSet
        macro_1_downbids = DataVector([bid2, bid6])
        correct_bids13 = all([bid in macro_area_1.down_bids for bid in macro_1_downbids])
        correct_bids14 = len(macro_area_1.down_bids) == len(macro_area_1.down_bids) and type(macro_area_1.down_bids) is BidSet

        reg_1_bids = DataVector([bid1, bid5])
        correct_bids15 = all([bid in reg_1.all_bids for bid in reg_1_bids])
        correct_bids16 = len(reg_1.all_bids) == len(reg_1_bids) and type(reg_1.all_bids) is BidSet

        correct_bids17 = all([bid in reg_1.up_bids for bid in reg_1_bids])
        correct_bids18 = len(reg_1.up_bids) == len(reg_1_bids) and type(reg_1.up_bids) is BidSet

        assert all([correct_bids1, correct_bids2, correct_bids3, correct_bids4, correct_bids5, correct_bids5,
                    correct_bids6, correct_bids7, correct_bids8, correct_bids9, correct_bids10, correct_bids11,
                    correct_bids12, correct_bids13, correct_bids14, correct_bids15, correct_bids16,
                    correct_bids17, correct_bids18])


"""Purpose: Test bidset object"""


class TestBidDistribution:

    def test_bidset_iteration(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)

        bid1 = Bid(_id=1, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=reg_1, hour_from=2, hour_to=5, volume=15, price=9, min_vol=4)
        bid3 = Bid(_id=3, direction=1, region=reg_1, hour_from=3, hour_to=4, volume=20, price=8, min_vol=3)
        bidset = BidSet(time_holder, [bid1, bid2, bid3])
        bids = DataVector()
        for bid in bidset:
            bids.add_object(bid)

        assert all([bid1 in bids, bid2 in bids, bid3 in bids])

    def test_bidset_remove(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)

        bid0 = Bid(_id=1, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid1 = Bid(_id=2, direction=2, region=reg_1, hour_from=1, hour_to=5, volume=15, price=9, min_vol=4)
        bid2 = Bid(_id=3, direction=1, region=reg_1, hour_from=2, hour_to=4, volume=20, price=8, min_vol=3)
        bid3 = Bid(_id=4, direction=1, region=reg_1, hour_from=2, hour_to=2, volume=10, price=10, min_vol=2)
        bid4 = Bid(_id=5, direction=2, region=reg_1, hour_from=2, hour_to=5, volume=15, price=9, min_vol=4)

        bidset = BidSet(time_holder, [bid0, bid1, bid2, bid3, bid4])

        bidset.remove_bid(bid3)
        assert bid3 not in bidset and bid3 not in bidset[2]

    def test_bidset_volume(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)

        bid0 = Bid(_id=1, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid1 = Bid(_id=2, direction=2, region=reg_1, hour_from=1, hour_to=5, volume=15, price=9, min_vol=4)
        bid2 = Bid(_id=3, direction=1, region=reg_1, hour_from=2, hour_to=4, volume=20, price=8, min_vol=3)

        bidset = BidSet(time_holder, DataVector([bid0, bid1, bid2]))
        bidset2 = BidSet(time_holder)
        assert (bidset.volume == bid0.volume + bid1.volume + bid2.volume and bidset2.volume == 0)

    def test_bidset_object(self):

        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder)

        bid0 = Bid(_id=1, direction=2, region=reg_1, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid1 = Bid(_id=2, direction=1, region=reg_1, hour_from=2, hour_to=4, volume=15, price=9, min_vol=4)
        bid2 = Bid(_id=3, direction=2, region=reg_1, hour_from=3, hour_to=4, volume=20, price=8, min_vol=3)
        bid3 = Bid(_id=4, direction=1, region=reg_1, hour_from=2, hour_to=3, volume=10, price=10, min_vol=2)
        bid4 = Bid(_id=5, direction=1, region=reg_1, hour_from=1, hour_to=2, volume=15, price=9, min_vol=4)

        bidset = BidSet(time_holder, [bid0, bid1, bid2, bid3, bid4])
        up_bidset = bidset.sub_bid_set(lambda bid: bid.direction == direction_holder.up)
        down_bidset = bidset.sub_bid_set(lambda bid: bid.direction == direction_holder.down)
        upbids = DataVector([bid1, bid3, bid4])
        downbids = DataVector([bid0, bid2])
        allbids = DataVector.union([upbids, downbids])
        hour_sep = time_holder.hour_range(DataVector)
        hour_sep[1].add_object([bid0, bid4])
        hour_sep[2].add_object([bid1, bid3])
        hour_sep[3].add_object([bid1, bid2])

        hour_separation = True
        for h in time_holder.hours:
            if not all(bid in bidset[h] for bid in hour_sep[h]):
                hour_separation = False

        union = all(bid in bidset for bid in allbids)
        union_up = all([bid in up_bidset for bid in upbids])
        no_intersection_up = all([bid not in down_bidset for bid in upbids])
        union_down = all([bid in down_bidset for bid in downbids])
        no_intersection_down = all([bid not in up_bidset for bid in downbids])

        assert all([hour_separation, union, union_up, union_down, no_intersection_down, no_intersection_up])


"""Purpose: Test xpress logic"""


class TestXpressModelLogic:

    def test_xpress_error(self):
        model = Model('model')
        a = Variable()
        b = Variable()
        a.create_bin_var(name='a')
        b.create_bin_var(name='a')
        model.add_variable(a, b)
        with pytest.raises(XpressError):
            model.add_variables()

    def test_set_gap(self):
        model = Model(initial_gap=0.1, gap_increment=0.1, gap_interval=0.1)
        model.set_gap()
        assert model.gap_control == 0.1

    def test_stepwise_gap(self):
        model = Model()
        callback = add_new_node_callback(time.time(), gap=0.1, interval=0.001, increment=0.05)
        import logging
        logger = logging.getLogger(__name__)
        callback(model.problem, logger, None, None, None)
        time.sleep(0.002)
        callback(model.problem, logger, None, None, None)
        time.sleep(0.002)
        callback(model.problem, logger, None, None, None)
        assert model.gap_control == 0.2

    def test_max_time(self):
        model = Model(max_time=100)
        assert (model.max_time == 100)

    def test_stop_model(self):
        time_holder = TimeHolder(1)
        up = 1
        down = 2
        penalty = 9999
        direction_holder = DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)
        reg_1 = Region(1, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        reg_2 = Region(2, time_holder, direction_holder, prod_min_penalty=penalty, demand_penalty=penalty)
        Connection(reg_1, reg_2, 100, cost=10)
        Connection(reg_2, reg_1, 100, cost=10)
        all_regions = [reg_1, reg_2]
        macro_area_1 = MacroArea(1, time_holder, direction_holder)
        macro_area_1.regions = all_regions
        nordic_area = NordicArea(time_holder, direction_holder, all_regions, macro_area_1)
        macro_area_1.set_prod_min(5, 1, up)
        macro_area_1.set_prod_max(20, 1, up)
        reg_2.set_prod_min(5, 1, down)
        reg_2.set_demand(1, 7, down)
        reg_1.set_demand(1, 4, up)
        reg_2.set_prod_max(10, 1, down)
        model = ModelClearing()
        model.nordic_area = nordic_area
        model.set_up()

        def worker1(_model, ):
            model.clearmarket()

        def worker2(_model):
            model.stop_model()

        t1 = threading.Thread(target=worker1, args=(model,))
        t2 = threading.Thread(target=worker2, args=(model,))
        t2.start()
        t1.start()
        t1.join()
        t2.join()
        response = model.get_status()
        assert model.finished and model.stopped and response['GAP'] == 0


"""Purpose: Test datavector object"""


class TestDataVector:

    def test_max_obj(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg, hour_from=1, hour_to=2, volume=10, price=10, min_vol=2)
        bid2 = Bid(_id=2, direction=2, region=reg, hour_from=2, hour_to=5, volume=25, price=11, min_vol=4)
        bid3 = Bid(_id=3, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=8, min_vol=3)
        set1 = DataVector([bid1, bid2, bid3])
        max_bid = set1.get_max_obj(lambda bid: bid.price)
        assert max_bid == bid2

    def test_sorted(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg, hour_from=1, hour_to=2, volume=10, price=12, min_vol=12)
        bid2 = Bid(_id=2, direction=2, region=reg, hour_from=2, hour_to=5, volume=25, price=11, min_vol=11)
        bid3 = Bid(_id=3, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=14, min_vol=14)
        bid4 = Bid(_id=4, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=9, min_vol=9)
        set1 = DataVector([bid1, bid2, bid3, bid4])
        old_set = DataVector([bid1, bid2, bid3, bid4])
        set2 = set1.sort(lambda bid: bid.price)
        sorted = DataVector([bid4, bid2, bid1, bid3])
        assert all([set2 == sorted, old_set == set1])

    def test_intersection(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg, hour_from=1, hour_to=2, volume=10, price=12, min_vol=12)
        bid2 = Bid(_id=2, direction=2, region=reg, hour_from=2, hour_to=5, volume=25, price=11, min_vol=11)
        bid3 = Bid(_id=3, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=14, min_vol=14)
        bid4 = Bid(_id=4, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=9, min_vol=9)

        set1 = DataVector([bid1, bid2])
        set2 = DataVector([bid1, bid3, bid4])

        intersection = DataVector.intersection([set1, set2])
        intersection2 = DataVector.intersection([set1])

        assert all([len(intersection) == 1, bid1 in intersection, len(intersection2) == 2])

    def test_complement(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg, hour_from=1, hour_to=2, volume=10, price=12, min_vol=12)
        bid2 = Bid(_id=2, direction=2, region=reg, hour_from=2, hour_to=5, volume=25, price=11, min_vol=11)
        bid3 = Bid(_id=3, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=14, min_vol=14)
        bid4 = Bid(_id=4, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=9, min_vol=9)

        set1 = DataVector([bid1, bid2])
        set2 = DataVector([bid1, bid3, bid4])

        complement = DataVector.difference([set1, set2])
        assert all([len(complement) == 3, bid2 in complement, bid3 in complement, bid4 in complement])

    def test_remove(self):
        time_holder = TimeHolder(4)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg = Region('NO1', time_holder, direction_holder)
        bid1 = Bid(_id=1, direction=1, region=reg, hour_from=1, hour_to=2, volume=10, price=12, min_vol=12)
        bid2 = Bid(_id=2, direction=2, region=reg, hour_from=2, hour_to=5, volume=25, price=11, min_vol=11)
        bid3 = Bid(_id=3, direction=1, region=reg, hour_from=2, hour_to=4, volume=20, price=14, min_vol=14)
        holder = DataVector([bid1, bid2, bid3])
        holder.remove_object(bid3)
        assert all([bid3 not in holder, bid1 in holder, bid2 in holder])

    def test_init(self):
        vector = [1, 2, 3]
        vector2 = ['k', 'j']
        assert DataVector(vector).elements == vector and DataVector(vector2).elements == vector2

    def test_empty(self):
        vector = DataVector()
        empty = vector.empty
        vector.add_object(1)
        not_empty = not vector.empty
        assert (empty and not_empty)

    def test_add_object(self):
        element = [1, 2]
        element2 = 4
        vector = DataVector()
        vector2 = DataVector()
        vector.add_object(element)
        vector2.add_object(element2)
        assert vector.elements == element and vector2.elements[0] == element2

    def test_get_max(self):
        vector = DataVector([1, 2, 10, 3, 2])
        assert vector.get_max(lambda x: x) == 10

    def test_unique(self):
        vector1 = DataVector([1, 3, 4, 5, 6, 7])
        vector2 = DataVector([1, 3, 4, 4, 5, 5, 6, 6, 7, 7])
        assert vector1 == vector2.unique()

    def test_get_element(self):
        id3 = 'no3'
        time_holder = TimeHolder(2)
        direction_holder = DirectionHolder(up=1, down=2, up_code=UP, down_code=DOWN)
        reg3 = Region(id3, time_holder, direction_holder)
        regions = [Region('no1', time_holder, direction_holder), reg3, Region('no2', time_holder, direction_holder)]
        vector = DataVector(regions)
        assert vector.get_element(id3) == reg3
