# Module bidSelectionModel
# Holds the class that defines the optimization model to select among competing bids

from .loggers import *
from .data_objects import *
from .basemodel import Model, Solution
import logging
from .utilities import DataVector
from datetime import datetime
logger = logging.getLogger(__name__)

"""Purpose: Class that sets up optimization problem for various bid selection problems
The class takes a set of macroareas and initializes the available bids in the macroareas,
the class provides several methods to setup the optimization problem
Input: DataVector of MacroArea"""


class BidSelectorModel(Model):
    def __init__(self, nordic_area, name="bidSelectorModel", random_seed=0, initial_gap=0, gap_interval=0,
                 gap_increment=0, max_time=None, run_id=None, soft=True):
        self.soft = soft
        self.logger = LoggerAdapter(logger, run_id)
        self.logger.info('Setting up model')
        self.nordic_area = nordic_area
        self.hours = self.nordic_area.hours
        self.time_holder = self.nordic_area.time_holder
        self.macro_areas = self.nordic_area.macro_areas
        self.regions = self.nordic_area.regions

        super(BidSelectorModel, self).__init__(name, BidSolution(self.time_holder, nordic_area.instance_id),
                                               random_seed, initial_gap, gap_interval, gap_increment, max_time,
                                               run_id=run_id)

        self.connections = DataVector([connection for region in self.regions for connection in region.OutEdges if
                                       connection.to_area in self.regions])

        self.linked_bids = self.nordic_area.linked_bids
        self.exclusive_bids = self.nordic_area.exclusive_bids
        self.all_bids = self.nordic_area.all_bids
        self.up_bids = self.nordic_area.up_bids
        self.down_bids = self.nordic_area.down_bids

        self.logger.info('Creating bid variables')
        for bid in self.all_bids:
            bid.accept_bid_var.create_bin_var(name=bid.name)
            bid.volume_var.create_int_var(name=bid.name + '_vol', upper_bound=bid.volume)
            self.add_variable(bid.accept_bid_var, bid.volume_var)

        self.init_flow_variables()
        if self.soft:
            self.init_penalty_variables()

    def solve(self):
        self.logger.info('Adding infeasiblity listener')
        self.add_infeasibility_listener(handle_infeasibilties)
        self.logger.info('Solves problem for ' + str(len(self.all_bids)) + ' bids')
        super(BidSelectorModel, self).solve()
        if not self.solution.sol_found:
            if not self.solution.feasible:
                self.solution.set_infeasible_string(self.solution.no_solution_info[INFO_STRING])
            return self.solution
        else:
            self.logger.info('Setting solution volumes to bid objects')
            for bid in self.all_bids:
                if bid.accept_bid_var.solution_val >= ZERO_TOLERANCE:
                    self.solution.accepted_bids.add_bid(bid)
                    bid.set_cleared(True)
                    bid.volume_sol = bid.volume_var.solution_val*bid.quantity_factor

            for connection in self.connections:
                for hour in self.hours:
                    if connection[hour].agg_volume.solution_val >= ZERO_TOLERANCE:
                        connection[hour].volume_sol = connection[hour].agg_volume.solution_val
        self.solution.set_sol_type_status()
        relaxed, relaxed_region_info, relaxed_macro_info = self.relaxed
        if relaxed:
            self.solution.set_relaxed(relaxed, relaxed_region_info, relaxed_macro_info)
        return self.solution

    @property
    def relaxed(self):
        relaxed = False
        relaxed_region_info = []
        relaxed_macro_info = []

        def relaxed_region(_hour, _region, direction):
            nonlocal relaxed, relaxed_region_info
            relaxed = True
            relaxed_region_info.append(
                {
                 HOUR: _hour,
                 REGION: _region.id,
                 DIRECTION: direction,
                }
            )
            if direction == region.direction_holder.up:
                if _region.demand_penalty_var[_hour].up.solution_val >= ZERO_TOLERANCE:
                    relaxed_region_info[-1][DEMAND] = _region.demand[_hour].up - \
                                                      _region.demand_penalty_var[_hour].up.solution_val
                if _region.prod_min_var[_hour].up.solution_val >= ZERO_TOLERANCE:
                    relaxed_region_info[-1][PROD_MIN] = _region.prod_min[_hour].up - _region.prod_min_var[_hour].up.solution_val
            else:
                if region.demand_penalty_var[_hour].down.solution_val >= ZERO_TOLERANCE:
                    relaxed_region_info[-1][DEMAND] = _region.demand[_hour].down \
                                                      - region.demand_penalty_var[_hour].down.solution_val
                if _region.prod_min_var[_hour].down.solution_val >= ZERO_TOLERANCE:
                    relaxed_region_info[-1][PROD_MIN] = _region.prod_min[_hour].down-_region.prod_min_var[_hour].down.solution_val

        def relaxed_macro_area(_hour, _area, direction):
            nonlocal relaxed, relaxed_macro_info
            relaxed = True
            relaxed_macro_info.append(
                {
                 HOUR: _hour,
                 MACRO_AREA: _area.id,
                 DIRECTION: direction,
                 PROD_MIN: 0
                }
            )
            if direction == area.direction_holder.up:
                relaxed_macro_info[-1][PROD_MIN] = _area.prod_min[_hour].up - _area.prod_min_var[_hour].up.solution_val
            else:
                relaxed_macro_info[-1][PROD_MIN] = _area.prod_min[_hour].down - _area.prod_min_var[_hour].down.solution_val

        for hour in self.hours:
            for region in self.regions:
                if region.demand_penalty_var[hour].up.solution_val >= ZERO_TOLERANCE or \
                        region.prod_min_var[hour].up.solution_val >= ZERO_TOLERANCE:
                    relaxed_region(hour, region, region.direction_holder.up)
                if region.demand_penalty_var[hour].down.solution_val >= ZERO_TOLERANCE or \
                        region.prod_min_var[hour].down.solution_val >= ZERO_TOLERANCE:
                    relaxed_region(hour, region, region.direction_holder.down)
            for area in self.macro_areas:
                if area.prod_min_var[hour].up.solution_val >= ZERO_TOLERANCE:
                    relaxed_macro_area(hour, area, area.direction_holder.up)
                if area.prod_min_var[hour].down.solution_val >= ZERO_TOLERANCE:
                    relaxed_macro_area(hour, area, area.direction_holder.down)
        return relaxed, relaxed_region_info, relaxed_macro_info

    def init_penalty_variables(self):
        self.logger.info('Creating penalty variables')
        for hour in self.hours:
            for region in self.regions:
                var1 = region.demand_penalty_var[hour].up.create_int_var(name='pen_up_'+str(region.id)+'_h_'+str(hour))
                var2 = region.demand_penalty_var[hour].down.create_int_var(name='pen_down_' + str(region.id) + '_h_' +
                                                                                str(hour))
                var3 = region.prod_min_var[hour].up.create_int_var(name='prodmin_up_reg_' +
                                                                        str(region.id)+'_h_'+str(hour))
                var4 = region.prod_min_var[hour].down.create_int_var(name='prodmin_down_reg_' +
                                                                          str(region.id) + '_h_' + str(hour))

                self.add_variable(var1, var2, var3, var4)

            for area in self.macro_areas:
                var1 = area.prod_min_var[hour].up.create_int_var(name='prodmin__up_area_' +
                                                                      str(area.id) + '_h_' + str(hour))
                var2 = area.prod_min_var[hour].down.create_int_var(name='prodmin_down_area_' +
                                                                        str(area.id) + '_h_' + str(hour))

                self.add_variable(var1, var2)

    """ Purpose: initializes the flow variables. The flow variables are attached to connection data objects"""
    def init_flow_variables(self):
        self.logger.info('Creating flow variables')
        for connection in self.connections:
            for hour in connection.from_area.hours:
                connection[hour].agg_volume.create_cont_var(name=connection.name + '_agg_h_' + str(hour), lower_bound=0,
                                                           upper_bound=connection[hour].capacity)

                connection[hour].up.create_cont_var(name=connection.name + '_up_h_' + str(hour), lower_bound=0,
                                                   upper_bound=connection[hour].capacity)

                connection[hour].down.create_cont_var(name=connection.name + '_down_h_' + str(hour), lower_bound=0,
                                                     upper_bound=connection[hour].capacity)
                self.add_variable(connection[hour].agg_volume, connection[hour].up, connection[hour].down)

    def add_energy_balance_constraint(self):
        self.logger.info('Creating energy balance constraints')
        for region in self.regions:
            for hour in self.hours:
                up_exp = self.get_sum([edge[hour].up.var for edge in region.OutEdges])
                up_imp = self.get_sum([edge[hour].up.var for edge in region.InEdges])
                down_exp = self.get_sum([edge[hour].down.var for edge in region.OutEdges])
                down_imp = self.get_sum([edge[hour].down.var for edge in region.InEdges])

                hard_up = (self.get_sum([bid.volume_var.var * bid.quantity_factor for
                                         bid in region.up_bids[hour]]) +
                           up_imp - up_exp - region.demand[hour].up)

                if self.soft:
                    up_ctr = self.constraint(hard_up + region.demand_penalty_var[hour].up.var >= 0,
                                             name='energy_bal_reg_' + str(region.id)+'_up_h_'+str(hour))
                else:
                    up_ctr = self.constraint(hard_up >= 0,
                                             name='energy_bal_reg_' + str(region.id) + '_up_h_' + str(hour))

                hard_down = (self.get_sum([bid.volume_var.var * bid.quantity_factor for
                                           bid in region.down_bids[hour]]) + down_imp - down_exp -
                             region.demand[hour].down)
                if self.soft:
                    down_ctr = self.constraint(hard_down + region.demand_penalty_var[hour].down.var >= 0,
                                               name='energy_bal_reg_' + str(region.id) + '_down_h_' + str(hour))
                else:
                    down_ctr = self.constraint(hard_down >= 0,
                                               name='energy_bal_reg_' + str(region.id) + '_down_h_' + str(hour))

                self.create_constraint([up_ctr, down_ctr])

    def add_flow_link(self):
        self.logger.info('Creating flow linking constraints')
        for connection in self.connections:
            for hour in self.hours:

                flow_link_up = self.constraint(connection[hour].agg_volume - connection[hour].up >= 0,
                                               name='flow_link_up_'+connection.name+'_h_' + str(hour))
                self.create_constraint(flow_link_up)

                to_region = connection.to_area
                rev_connection = to_region.get_connection(to_area=connection.from_area)
                if rev_connection is None:
                    raise ImplementationError('Default reverse connection should be defined')
                flow_link_down = self.constraint(connection[hour].agg_volume - rev_connection[hour].down >= 0,
                                                 name='flow_link_down_'+connection.name+'_h_' + str(hour))
                self.create_constraint(flow_link_down)

    def add_objective(self):
        self.logger.info('Creating objective')
        self.logger.info('Created bid cost for '+str(len(self.all_bids)) + ' bids')
        bid_cost = self.get_sum([bid.price*(bid.hour_to-bid.hour_from)*bid.quantity_factor *
                                 bid.volume_var.var for bid in self.all_bids])

        self.logger.info('Created bid cost')

        flow_cost = self.get_sum([connection[hour].agg_volume.var * connection[hour].cost for connection in
                                  self.connections for hour in self.hours])

        demand_penalty = 0
        prod_reg_penalty = 0
        prod_macro_penalty = 0
        if self.soft:
            demand_penalty = self.get_sum([region.demand_penalty*region.demand_penalty_var[hour].down.var +
                                           region.demand_penalty*region.demand_penalty_var[hour].up.var
                                           for hour in self.hours for region in self.regions])

            prod_reg_penalty = self.get_sum([region.prod_min_penalty*(region.prod_min_var[hour].up +
                                            region.prod_min_var[hour].down)
                                             for region in self.regions
                                             for hour in self.hours])

            prod_macro_penalty = self.get_sum([area.prod_min_penalty * (area.prod_min_var[hour].up +
                                              area.prod_min_var[hour].down)
                                             for area in self.macro_areas
                                             for hour in self.hours])

        self.add_objective_element(bid_cost+demand_penalty+flow_cost+prod_reg_penalty+prod_macro_penalty)

    def add_regional_production_constraint(self):
        self.logger.info('Creating regional production constraints')
        for region in self.regions:
            for hour in self.hours:
                ctr = []
                hard_min_up = (self.get_sum([bid.volume_var*bid.quantity_factor for bid in region.up_bids[hour]]) -
                               region.prod_min[hour].up)
                if self.soft:
                    min_up_ctr = self.constraint(hard_min_up + region.prod_min_var[hour].up.var >= 0,
                                                 name='prod_min_'+str(region.up_code)+'_reg_'+str(region.id) +
                                                      '_h_'+str(hour))
                else:
                    min_up_ctr = self.constraint(hard_min_up >= 0,
                                                 name='prod_min_' + str(region.up_code) + '_reg_' + str(region.id) +
                                                      '_h_' + str(hour))

                ctr.append(min_up_ctr)

                hard_min_down = (self.get_sum([bid.volume_var * bid.quantity_factor for bid in region.down_bids[hour]])
                                 - region.prod_min[hour].down)

                if self.soft:
                    min_down_ctr = self.constraint(hard_min_down + region.prod_min_var[hour].down.var >= 0,
                                                   name='prod_min_' + str(region.down_code) + '_reg_' + str(region.id)
                                                        + '_h_' + str(hour))
                else:
                    min_down_ctr = self.constraint(hard_min_down >= 0,
                                                   name='prod_min_' + str(region.down_code) + '_reg_' + str(region.id)
                                                        + '_h_' + str(hour))
                ctr.append(min_down_ctr)

                if region.prod_max[hour].up is not None:
                    max_up_ctr = self.constraint(
                        self.get_sum([bid.volume_var * bid.quantity_factor for bid in region.up_bids[hour]]) -
                        region.prod_max[hour].up <= 0, name='prod_max_' + str(region.up_code) +
                                                            '_reg_'+str(region.id) + '_h_' + str(hour))
                    ctr.append(max_up_ctr)

                if region.prod_max[hour].down is not None:
                    max_down_ctr = self.constraint(
                        self.get_sum([bid.volume_var * bid.quantity_factor for bid in region.down_bids[hour]]) -
                        region.prod_max[hour].down <= 0, name='prod_max_' + str(region.down_code) +
                                                              '_reg_' + str(region.id) + '_h_' + str(hour))
                    ctr.append(max_down_ctr)

                self.create_constraint(ctr)

    def link_bid_variables(self):
        self.logger.info('Creating constraints to link bid variables')
        for bid in self.all_bids:
            ctr1 = self.constraint(bid.volume_var.var * bid.quantity_factor - bid.accept_bid_var.var * bid.min_vol >= 0,
                                   name='link_bid_min'+str(bid.id))
            ctr2 = self.constraint(bid.volume_var.var * bid.quantity_factor - bid.accept_bid_var.var * bid.volume <= 0,
                                   name='link_bid_max' + str(bid.id))
            self.create_constraint([ctr1, ctr2])

    def add_link_bids_constraint(self):
        self.logger.info('Creating constraints to link bid in linked bid groups')
        for group in self.nordic_area.linked_bids:
            for i in range(0, len(group)-1):
                bid1 = group[i]
                for j in range(i+1, len(group)):
                    bid2 = group[j]
                    ctr = self.constraint(bid1.accept_bid_var.var - bid2.accept_bid_var.var == 0,
                                          name='link_bid_'+str(bid1.id)+'_'+str(bid2.id))
                    self.create_constraint(ctr)

    def add_exclusive_bids_constraint(self):
        self.logger.info('Creating exclusive bid constraints')
        i = 1
        for group in self.nordic_area.exclusive_bids:
            ctr = self.constraint(self.get_sum([bid.accept_bid_var.var*bid.exclusive_bid_coefficient for bid in group])
                                  - 1 <= 0, name='exclusive_bid_group_'+str(i))
            i += 1
            self.create_constraint(ctr)

    def add_demand_cut(self):
        self.logger.info('Creating demand cut')
        for hour in self.hours:
            hard_up_cut = (self.get_sum([bid.volume_var.    var*bid.quantity_factor for bid in self.up_bids[hour]]) -
                           self.nordic_area.demand[hour].up())

            if self.soft:
                up_cut = self.constraint(hard_up_cut + self.get_sum([region.demand_penalty_var[hour].up.var
                                                                     for region in self.regions]) >= 0,
                                         name='up_cut_h_'+str(hour))
            else:
                up_cut = self.constraint(hard_up_cut >= 0,
                                         name='up_cut_h_' + str(hour))

            hard_down_cut = (self.get_sum([bid.volume_var.var*bid.quantity_factor for bid in self.down_bids[hour]]) -
                             self.nordic_area.demand[hour].down())

            if self.soft:
                down_cut = self.constraint(hard_down_cut + self.get_sum([region.demand_penalty_var[hour].down.var
                                                                         for region in self.regions]) >= 0,
                                           name='down_cut_h_' + str(hour))
            else:
                down_cut = self.constraint(hard_down_cut >= 0,
                                           name='down_cut_h_' + str(hour))

            self.create_constraint([up_cut, down_cut])

    def add_maco_production_constraints(self):
        self.logger.info('Creating macro area production constraints')
        for area in self.macro_areas:
            for hour in self.hours:
                ctr = []
                min_up_hard = (self.get_sum([bid.quantity_factor*bid.volume_var for bid in area.up_bids[hour]]) -
                               area.prod_min[hour].up)
                if self.soft:
                    min_up_ctr = self.constraint(min_up_hard + area.prod_min_var[hour].up.var >= 0,
                                                 name='prod_min_' + str(area.up_code) + '_area_' + str(area.id) +
                                                      '_h_' + str(hour))
                else:
                    min_up_ctr = self.constraint(min_up_hard >= 0,
                                                 name='prod_min_' + str(area.up_code) + '_area_' + str(area.id) +
                                                      '_h_' + str(hour))

                ctr.append(min_up_ctr)

                min_down_hard = (self.get_sum([bid.quantity_factor * bid.volume_var for bid in area.down_bids[hour]]) -
                                 area.prod_min[hour].down)

                if self.soft:
                    min_down_ctr = self.constraint(min_down_hard + area.prod_min_var[hour].down.var >= 0,
                                                   name='prod_min_' + str(area.down_code) + '_area_' +
                                                        str(area.id) + '_h_' + str(hour))
                else:
                    min_down_ctr = self.constraint(min_down_hard >= 0,
                                                   name='prod_min_' + str(area.down_code) + '_area_' +
                                                        str(area.id) + '_h_' + str(hour))

                ctr.append(min_down_ctr)

                if area.prod_max[hour].up is not None:
                    max_up_ctr = self.constraint(
                        self.get_sum([bid.volume_var * bid.quantity_factor for bid in area.up_bids[hour]]) -
                        area.prod_max[hour].up <= 0, name='prod_max_' + str(area.up_code) + '_area_'
                                                          + str(area.id) + '_h_' + str(hour))
                    ctr.append(max_up_ctr)

                if area.prod_max[hour].down is not None:
                    max_down_ctr = self.constraint(
                        self.get_sum([bid.volume_var * bid.quantity_factor for bid in area.down_bids[hour]]) -
                        area.prod_max[hour].down <= 0, name='prod_max_' + str(area.down_code) + '_area_'
                                                            + str(area.id) + '_h_' + str(hour))

                    ctr.append(max_down_ctr)

                self.create_constraint(ctr)


"""Purpose: Class that extends general solution. Includes json response methods"""


class BidSolution(Solution):
    messages = {CLEARED: 'RunId %s has been cleared.',
                OPTIMAL: 'Optimal solution has been found.',
                RELAXED: 'One or more constraints had to be relaxed to find a valid solution.',
                TIMEOUT: 'The algorithm timed out, so the solution found is not optimal.',
                TIMEOUT_NOSOL: 'RunId %s has not found any valid solution because of timeout.',
                INFEASIBLE: 'RunId %s is infeasible.'
                }
    SubStatus = 'SUB'
    FailStatus = 'FAIL'
    OptStatus = 'OPT'
    RelaxStatus = 'RELAX'
    TimeoutStatus = 'TIMEOUT'
    NoSol = 'NoSol'
    IntegerSol = 'IntSol'

    def __init__(self, time_holder, instance_id):
        super(BidSolution, self).__init__()
        self.accepted_bids = BidSet(time_holder)
        self._stage = ''
        self.runId = instance_id
        self.stage_description = ''
        self.no_sol_key = ''
        now = datetime.now()
        self.date = now.strftime('%Y.%m.%d')
        self.xpress_status = None
        self.stop_status = None
        self.Relaxed = False
        self.infString = 'No info'
        self.relaxed_region_info = []
        self.relaxed_macro_info = []

    def set_relaxed(self, ind, region_info=None, macro_info=None):
        self.Relaxed = ind
        if region_info is not None:
            self.relaxed_region_info = region_info
        if macro_info is not None:
            self.relaxed_macro_info = macro_info

    def set_infeasible_string(self, str):
        self.infString = str

    def set_sol_type_status(self):
        if self.gap > self.gap_control + ZERO_TOLERANCE:
            self.set_stop_status(BidSolution.SubStatus)
        else:
            self.set_stop_status(BidSolution.OptStatus)

    def set_stop_status(self, status):
        self.stop_status = status

    def get_current_best_sol_info(self):
        gap = -1
        sol_time = -1
        ub = -1
        if self.gap is not None:
            gap = self.gap
            sol_time = self.solution_time
            ub = self.ub

        return {
            'GAP': gap,
            'UPPERBOUND': ub,
            'RUNTIME': sol_time
        }

    def get_report(self):
        report = {STATUS: None,
                  LOG: [],
                  }

        if not self.feasible:
            report[STATUS] = BidSolution.FailStatus
            report[LOG].append(BidSolution.messages[INFEASIBLE] % self.runId)
            report[LOG].append(self.infString)

        else:
            report[DURATION] = round(self.solution_time, 4)

            if self.status == Solution.NO_SOL:
                report[STATUS] = BidSolution.TimeoutStatus
                report[LOG].append(BidSolution.messages[TIMEOUT_NOSOL] % self.runId)

            elif self.Relaxed:
                report[STATUS] = self.stop_status + '_' + BidSolution.RelaxStatus
                report[LOG].append(BidSolution.messages[CLEARED] % self.runId)
                if self.stop_status == BidSolution.SubStatus:
                    report[LOG].append(BidSolution.messages[TIMEOUT])

                report[LOG].append(BidSolution.messages[RELAXED])
                # report['log'].append('Relaxed in (hour, region, direction, volume(MW)): ')
                # for tup in self.relaxed_region_info:
                #    report['log'][-1] += str(tup)

            else:
                report[STATUS] = self.stop_status
                report[LOG].append(BidSolution.messages[CLEARED] % self.runId)
                if self.stop_status == BidSolution.SubStatus:
                    report[LOG].append(BidSolution.messages[TIMEOUT])
                else:
                    report[LOG].append(BidSolution.messages[OPTIMAL])

        return report


'''Purpose: Infeasibility handler. Meant to register as infeasibility listener on model instance.
Xpress specific implementation. 
Input: Xpress problem. Provided by the model instance in callback.
Ouput: Infeasibility response as dict'''


def handle_infeasibilties(problem):
    miisrow = []
    miiscol = []
    constrainttype = []
    colbndtype = []
    duals = []
    rdcs = []
    isolationrows = []
    isolationcols = []
    i = 0
    info_string = 'Infeasible in hour: '
    prev_hour = -1
    ctr_holder = []
    inf_hours = []

    while problem.iisnext() == 0:
        i += 1
        problem.getiisdata(i, miisrow, miiscol, constrainttype, colbndtype, duals, rdcs, isolationrows,
                           isolationcols)

        hour = None
        for ctr in miisrow:
            pos = str(ctr).find('h_')
            part1 = str(ctr)[pos + 2]
            part2_exists = True
            part2 = None
            try:
                part2 = str(ctr)[pos+3]
            except IndexError:
                part2_exists = False

            if part2_exists:
                hour = int(part1+part2)
            else:
                hour = int(part1)

            ctr_holder.append(str(ctr) + ': ' + str(ctr.body) + ' >= ' + str(ctr.lb) + ' and <=' + str(ctr.ub))

        if prev_hour != hour:
            inf_hours.append({HOUR: hour, RESTRICTIONS: ctr_holder})
            ctr_holder = []

        prev_hour = hour

    inf_hours = sorted(inf_hours, key=lambda inf_hour: inf_hour[HOUR])
    for hour in range(0, len(inf_hours)):
        info_string += str(inf_hours[hour][HOUR])
        if hour == len(inf_hours) - 2:
            info_string += ' and '
        elif not hour == len(inf_hours)-1:
            info_string += ', '

    info_string += ' due to insufficient volume available.'

    output = infeasibleOutput
    output[INFO_STRING] = ''.join(info_string)
    output[INF_HOURS] = inf_hours
    return output
