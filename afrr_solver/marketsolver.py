from .database_initializer import Database
from .afrr_errors import *
from .model_classes import BidSelectorModel
from .basemodel import is_system_error
from .loggers import *
import logging
from .constants import *
from copy import copy
from .basemodel import check_licence

logger = logging.getLogger(__name__)


class ModelClearing:
    def __init__(self, run_id=0, json_data=None):
        self.logger = LoggerAdapter(logger, run_id)
        self.stopwatch_logger = StopWatchAdapter(run_id)
        self.data = json_data
        self.run_id = run_id
        self.nordic_area = None
        self.model = None
        self.db = None
        self.solution = None
        self.finished = False
        self.optimality_gap = 0
        self.gap_interval = 0
        self.gap_increment = 0
        self.gap_interval = 1000
        self.random_seed = 0
        self.max_time = None

    @property
    def stopped(self):
        return self.model.stop_solve

    def initialize(self):
        self.logger.info('Initializing database')

        db = Database(self.data, self.run_id)
        self.db = db
        if db.initialized:
            self.nordic_area = self.db.nordic_area
            self.optimality_gap = self.db.optimality_gap
            self.random_seed = self.db.random_seed
            self.gap_interval = self.db.gap_interval
            self.gap_increment = self.db.gap_increment
            self.max_time = self.db.max_time
        else:
            raise InputError('Data is None')

    def set_up(self, soft=True):
        self.stopwatch_logger.start()
        try:
            self.model = BidSelectorModel(nordic_area=self.nordic_area, random_seed=self.random_seed,
                                          initial_gap=self.optimality_gap, gap_interval=self.gap_interval,
                                          gap_increment=self.gap_increment, max_time=self.max_time,
                                          run_id=self.run_id, soft=soft)
            self.solution = self.model.solution
            self.model.add_variables()
            self.model.add_energy_balance_constraint()
            self.model.link_bid_variables()
            self.model.add_flow_link()
            self.model.add_maco_production_constraints()
            self.model.add_regional_production_constraint()
            self.model.add_demand_cut()
            self.model.add_link_bids_constraint()
            self.model.add_exclusive_bids_constraint()
            self.model.add_constraints()
            self.model.add_objective()
            self.model.set_objective()
        except XpressError as e:
            if is_system_error(e):
                raise SystemError('Xpress system error: ' + str(e))
            raise ImplementationError('Algorithm implementation error. Xpress error: ' + str(e))
        self.stopwatch_logger.stop()
        self.stopwatch_logger.info('Setting up xpress problem')

    def clearmarket(self):
        self.solve()
        return self.prepare_json_response()

    def solve(self):
        self.stopwatch_logger.start()
        try:
            self.model.solve()
        except XpressError as e:
            if is_system_error(e):
                raise SystemError('Xpress system error: ' + str(e))
            raise ImplementationError('Algorithm implementation error. Xpress error: ' + str(e))
        self.finished = True
        self.stopwatch_logger.stop()
        self.stopwatch_logger.info('Running xpress solver')
        if not self.solution.sol_found:
            if not self.solution.feasible:
                return self.solution
        return self.solution

    def stop_model(self):
        self.logger.info('Called stop on model. Waiting for initialization of model object')
        while self.model is None:
            time.sleep(0.1)
        self.logger.info('Stop signal registered on model, waiting for xpress interuption')
        self.model.stop_solve = True
        while not self.finished:
            time.sleep(0.1)
        self.logger.info('Xpress interrupted, returning termination response')
        return self.prepare_json_response()

    def get_status(self):
        return self.solution.get_current_best_sol_info()

    def prepare_json_response(self):
        self.logger.info('Preparing response')
        self.stopwatch_logger.start()
        report = self.solution.get_report()
        return_format = copy(RETURN_FORMAT)
        if not self.solution.feasible:
            return_format[ID] = self.run_id
            return_format[STATUS] = report[STATUS]
            return_format[LOG] = report[LOG]
            return return_format

        return_format[ID] = self.nordic_area.instance_id
        return_format[STATUS] = report[STATUS]
        return_format[LOG] = report[LOG]
        return_format[OBJECTIVE_VALUE] = round(self.solution.objective_value, 2)
        return_format[TOTAL_COST] = round(self.nordic_area.total_cost, 2)
        return_format[OPTIMALITY_GAP] = round(self.solution.gap, 17)
        return_format[DURATION] = round(self.solution.solution_time, 2)
        return_format[BIDS] = self.nordic_area.all_bids.to_dict()
        return_format[CONNECTION_CAPACITY] = self.nordic_area.connection_info
        return_format[REGION_DEMAND_REL] = self.solution.relaxed_region_info
        return_format[MACRO_DEMAND_REL] = self.solution.relaxed_macro_info
        self.stopwatch_logger.stop()
        self.stopwatch_logger.info('Preparing json response')
        return return_format


def checklicense():
    check_licence()
