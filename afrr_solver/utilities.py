import json
from collections import Iterable


class DataVector:
    def __init__(self, initial_elements = []):
        self._elements = []
        self.elements = initial_elements

    def sort(self, key_func):
        return DataVector(sorted(self, key= key_func))

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, elements):
        if not isinstance(elements, Iterable):
            raise Exception('Elements must be iterable')
        [self._elements.append(element) for element in elements]

    def map(self, func):
        [func(element) for element in self]

    def get_max(self, func):
        try:
            return max([func(obj) for obj in self])
        except:
            # TODO better handling
            return None

    @property
    def empty(self):
        if len(self) == 0:
            return True
        return False

    def get_max_obj(self, func):
        m = -float('Inf')
        maxObj = None
        for obj in self:
            if func(obj) >= m:
                maxObj = obj
                m = func(obj)
        return maxObj

    @classmethod
    def complement(cls, all_elements, selected_elements, key_func=None):
        _complement = DataVector()
        if key_func is not None:
            for element in all_elements:
                if not selected_elements.infunc(element, key_func):
                    _complement.add_object(element)
        else:
            for element in all_elements:
                if element not in selected_elements:
                    _complement.add_object(element)
        return _complement

    @classmethod
    def difference(cls, DataVectors, key_func=None):
        all_elements = cls.union(DataVectors).unique()
        _intersection = cls.intersection(DataVectors, key_func)
        return cls.complement(all_elements, _intersection, key_func)

    @classmethod
    def intersection(cls, DataVectors, key_func=None):
        all_elements = cls.union(DataVectors).unique(key_func)
        _intersection = DataVector()
        for element in all_elements:
            _intersect = True
            if key_func is not None:
                for dv in DataVectors:
                    if not dv.infunc(element, key_func):
                        _intersect = False
            else:
                for dv in DataVectors:
                    if element not in dv:
                        _intersect = False
            if _intersect:
                _intersection.add_object(element)
        return _intersection

    def unique(self, key_func=None):
        unique = DataVector()
        if key_func is not None:
            _keys = DataVector()
            for obj in self:
                if not key_func(obj) in _keys:
                    unique.add_object(obj)
                    _keys.add_object(key_func(obj))
        else:
            for obj in self:
                if not obj in unique:
                    unique.add_object(obj)
        return unique

    def print(self, func, path):
        output = [element for obj in self for element in func(obj)]
        #pd.DataFrame(output).to_csv(path)

    def remove_object(self, object):
        for i in range(0, len(self)):
            if self[i].name == object.name:
                del self.elements[i]
                break
            i += 1

    def add_object(self, new_object, flattening=True):
        if isinstance(new_object, Iterable) and flattening:
            [self.elements.append(element) for element in new_object]
        else:
            self.elements.append(new_object)
        if len(self.elements) > 0:
            return self.elements[-1]

    def get_element(self, id):
        for obj in self:
            if obj.id == id:
                return obj

    def getSum(self, getFunc):
        return sum([getFunc(obj) for obj in self])

    def inVector(self, obj):
        for data in self:
            if obj == data:
                return True
        return False

    def __str__(self):
        _str = ''
        for _obj in self:
            _str += str(_obj)
        return _str

    def __iter__(self):
        return iter(self.elements)

    def __getitem__(self, item):
        return self.elements[item]

    def __len__(self):
        return len(self.elements)

    def __eq__(self, other):
        return self.elements == other.elements

    @classmethod
    def union(cls, DataVectors):
        _union = cls([obj for vector in DataVectors for obj in vector.elements])
        return _union

    def getAll(self, getFunc):
        return DataVector.union([getFunc(obj) for obj in self])

    def get_members(self, getFunc):
        return [getFunc(obj) for obj in self]

    def infunc(self, element, key_func):
        _keys = [key_func(_obj) for _obj in self]
        if key_func(element) in _keys:
            return True
        return False

    def filter_object(self, boolfunc):
        for value in self:
            if boolfunc(value):
                return value

    def filter(self, boolFunc):
        vec = DataVector()
        [vec.add_object(val) for val in self if boolFunc(val)]
        return vec

    def get_object(self, id):
        for element in self.elements:
            if id == element.id:
                return element
        # TODO should warn
        return None


def func_closure(func, obj):
    def wrapper():
        nonlocal obj
        func(obj)

    return wrapper


def callback(func, *args):
    def cb():
        return func(*args)
    return cb


def value_checker(value, *funcs):
    for func in funcs:
        func(value)
    return value


def write_json(sample, filename):
    with open(filename+'.json', 'w') as fp:
        json.dump(sample, fp)


class DictHelper:
    def __init__(self, obj_type, *args):
        self.args = args
        self.obj_type = obj_type
        self.dict_holder = {}

    @property
    def keys(self):
        return self.dict_holder.keys()

    def __getitem__(self, item):
        if item in self.dict_holder.keys():
            return self.dict_holder[item]
        else:
            self.dict_holder[item] = self.obj_type(*self.args)
            return self.dict_holder[item]


def openjson(jsonfile):
    """openjson reads and stores the jsonfile as(data)"""

    try:
        with open(jsonfile) as f:
            data = json.load(f)

        return data
    except:
        raise Exception


def iterator(container):
    if not isinstance(container, Iterable):
        container = DataVector([container])
    return container
