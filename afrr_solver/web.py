import os
import time
import traceback
from flask import Flask, jsonify, request
from flask_api import status
from .afrr_errors import LicenseError, InputError, ImplementationError
from .marketsolver import ModelClearing, checklicense
from .setup_logging import setup_logging
from .loggers import LoggerAdapter
import logging

base_logger = logging.getLogger(__name__)
app = Flask(__name__)
env = os.environ

solver_version = env.get('SOLVER_VERSION', 'unknown')
debug_enabled = env.get('ENABLE_DEBUG') in ('True', 'true')
threading_enabled = env.get('ENABLE_FLASK_THREADING') in ('True', 'true')
port_number = int(env.get('SOLVER_PORT', 6000))


def create_termination_callback(model):
    def terminate():
        preliminary_result = model.stop_model()
        return jsonify({
                'status': 'ABORTED',
                'preliminary_result': preliminary_result,
            })
    return terminate

def create_get_status_callback(model):
    def get_status():
        status = model.get_status()
        return jsonify({
            'Best solution gap': status['GAP'],
            'Upper bound': status['UPPERBOUND'],
            'Xpress runtime': status['RUNTIME']
        })

    return get_status



# Initialized at runtime after model creation
def terminate_model():
    return jsonify({
        'status': 'ABORTED',
        'preliminary_result': -1,
    })

def get_status_model():
    return jsonify({
        'Best solution gap': -1,
        'Upper bound': -1,
        'Xpress runtime': -1
    })

# run the market clearing solver
@app.route('/solve', methods=['POST'])
def calc():
    time_start = time.time()
    if request.content_type is None or request.content_type.split(';')[0] != "application/json":
        logger = LoggerAdapter(logger=base_logger, run_id=None)
        logger.warning("solve request rejected, Content-type must be application/json, was: " + str(request.content_type))
        return jsonify({
            'status': 'FAILED',
            'error': 'InputError',
            'message': 'Content-type must be application/json, was: ' + str(request.content_type)
        }), status.HTTP_400_BAD_REQUEST
    try:
        data = request.get_json()
        run_id = data['id']
        logger = LoggerAdapter(base_logger, run_id)
        model = ModelClearing(run_id, data)

        global terminate_model, get_status_model
        terminate_model = create_termination_callback(model)
        get_status_model = create_get_status_callback(model)
        model.initialize()
        model.set_up()
        res = model.clearmarket()
        if model.solution.no_sol:
            return jsonify(res)

        time_end = time.time() - time_start
        logger.info("Completed solve operation, time spent=" + str(time_end))
        return jsonify({
                'id': run_id,
                'input': data,
                'result': res,
                'time': time_end
            })
    except InputError as inputErr:
        logger = LoggerAdapter(logger=base_logger, run_id=None)
        logger.warning('InputError:  %s\n%s'%(str(inputErr), traceback.format_exc()))
        return jsonify({
            'status': 'FAILED',
            'error': 'InputError',
            'message': str(inputErr)
        }), status.HTTP_400_BAD_REQUEST
    except ImplementationError as impErr:
        logger = LoggerAdapter(logger=base_logger, run_id=None)
        logger.error('ImplementationError: %s\n%s'%(str(impErr), traceback.format_exc()))
        return jsonify({
            'status': 'FAILED',
            'error': 'ImplementationError',
            'message': str(impErr)
        }), status.HTTP_500_INTERNAL_SERVER_ERROR
    except SystemError as systemErr:
        logger = LoggerAdapter(logger=base_logger, run_id=None)
        logger.error('SystemError: %s\n%s'%(str(systemErr), traceback.format_exc()))
        return jsonify({
            'status': 'FAILED',
            'error': 'SystemError',
            'message': str(systemErr)
        }), status.HTTP_500_INTERNAL_SERVER_ERROR
   # catch all
    except Exception as e:
        logger = LoggerAdapter(logger=base_logger, run_id=None)
        logger.error('UnknownError: %s\n%s'%(str(e), traceback.format_exc()))
        return jsonify({
            'status': 'FAILED',
            'error': 'UnknownError',
            'message': str(e)
        }), status.HTTP_500_INTERNAL_SERVER_ERROR



# @app.route('/terminate/<string:run_id>', methods=['GET'])
@app.route('/terminate')
def terminate():
    return terminate_model()

@app.route('/get_status')
def get_status():
    return get_status_model()


@app.route('/heartbeat')
def heartbeat():
    return jsonify({
        'APPLICATION': 'afrr-solver',
        'STATUS': 'ok',
        'SOLVER_VERSION': solver_version,
        'ENABLE_FLASK_THREADING': str(threading_enabled),
        'ENABLE_DEBUG': str(debug_enabled)
    })

@app.route('/checklicense')
def check_license():
    logger = LoggerAdapter(base_logger, run_id=None)
    logger.debug("checking license")
    try:
        checklicense()
    except LicenseError as e:
        logger.error("LicenseError: " + str(e))
        return jsonify({
            'status': 'FAILED',
            'error': 'LicenseError',
            'message': str(e)
        }), status.HTTP_503_SERVICE_UNAVAILABLE
    else:
        return jsonify({
            'status': 'OK'
        })


def start_service():
    setup_logging()
    logger = LoggerAdapter(base_logger, None)
    logger.info("----------------------------------------------------------")
    logger.info("Starting " + __name__ + " service")
    logger.info("Environment:")
    logger.info(" SOLVER_PORT=" + str(port_number))
    logger.info(" ENABLE_DEBUG=" + str(debug_enabled))
    logger.info(" ENABLE_FLASK_THREADING=" + str(threading_enabled))
    logger.info("----------------------------------------------------------")

    app.run(debug=debug_enabled, threaded=threading_enabled, port=port_number, host='0.0.0.0')
