
DOWNBIDS = 'downBids'
UPBIDS = 'upBids'

CONFIG = 'config'
NHOURS = 'numberOfHours'
NREGIONS = 'numberOfBiddingZones'
AREA_DEMAND = 'biddingZoneDemands'
HOUR = 'hour'
DIRECTION = 'direction'
DIRECTIONS = 'directions'
DEMAND = 'demand'
PROD_MIN = 'prodMin'
PROD_MAX = 'prodMax'
STOPWATCH_LOGGER = 'stoppeklokke'

RESTRICTIONS = 'restriksjoner'
INFO_STRING = 'informationString'
INF_HOURS = 'infeasibleTimer'
infeasibleOutput = {
            INFO_STRING: '',
            INF_HOURS: []
        }

OPTIMALITY_GAP = 'optimalityGap'
RANDOM_SEED = 'randomSeed'
GAP_INTERVAL = 'gapInterval'
GAP_INCREMENT = 'gapIncrement'
MAX_TIME = 'maxProcessingTime'

OBJECTIVE_VALUE = 'objectiveFunctionValue'
DURATION = 'optimizationDuration'
TOTAL_COST = 'totalCost'
BIDS = 'bids'
ID = 'id'
STATUS = 'status'
LOG = 'log'
CONNECTION_CAPACITY = 'transferCapacities'
REGION_DEMAND_REL = 'biddingZoneDemandsRelaxed'
MACRO_DEMAND_REL = 'macroAreaDemandsRelaxed'
RETURN_FORMAT = {
    ID: None,
    STATUS: None,
    LOG: None,
    'function': 'clearmarket',
    OBJECTIVE_VALUE: 0,
    TOTAL_COST: 0,
    OPTIMALITY_GAP: 0,
    DURATION: 0,
    BIDS: [],
    CONNECTION_CAPACITY: [],
    REGION_DEMAND_REL: [],
    MACRO_DEMAND_REL: []
}

HOUR_FROM = 'hourFrom'
HOUR_TO = 'hourTo'
MIN_VOL = 'minVolume'
VOLUME = 'volume'
ACCEPTED_VOLUME = 'acceptedVolume'
BID_UNSELECTED = 'I'
BID_SELECTED = 'V'
BID_SKIPPED = 'O'
PRICE = 'price'
LINKED_BIDS = 'linkedBids'
EXCLUSIVE_BIDS = 'exclusiveBids'
QUANTITY_FACTOR = 'quantityFactor'
PROD_MIN_PENALTY_MACRO = 'penaltyRelaxProdMinMacroArea'
PROD_MIN_PENALTY_REGION = 'penaltyRelaxProdMinBiddingZone'
DEMAND_PENALTY_REGION = 'penaltyRelaxDemandBiddingZone'


#Report message types
CLEARED = 'Cleared'
OPTIMAL = 'Optimal'
RELAXED = 'Relaxed'
TIMEOUT = 'Timeout'
TIMEOUT_NOSOL = 'TimeoutNoSol'
INFEASIBLE = 'Infeasible'

ZERO_TOLERANCE = 1e-6

UP = 'UP'
DOWN = 'DOWN'
NAME = 'name'

RETURN_BID_INFO = ''

REGION = 'biddingZone'
AREA_CODE = 'biddingZoneCode'
NAREAS = 'numberOfMacroAreas'
MACRO_AREA = 'macroArea'
MACRO_AREAS = 'biddingZonesMacroAreas'
AREA_PROD_REST = 'macroAreaDemands'


CAPACITY = 'capacity'
FROM_REGION = 'biddingZoneOut'
TO_REGION = 'biddingZoneIn'
RESERVERD_CAP = 'reservedCapacity'
COST = 'cost'

