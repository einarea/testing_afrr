import os
import yaml
import logging.config
import logging
import coloredlogs


def setup_logging(default_config='logging.yaml', default_level=logging.INFO, env_key='LOG_CFG'):
    if 'LOG_CONFIG' in os.environ:
        config_file = os.getenv(env_key, None)
    else:
        location = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        config_file = os.path.join(location, default_config)

    if os.path.exists(config_file):
        with open(config_file, 'rt') as f:
            try:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
                coloredlogs.install()
            except Exception as e:
                print(e)
                print('Error in logging configuration, using defaults')
                logging.basicConfig(level=default_level)
                coloredlogs.install(level=default_level)
    else:
        logging.basicConfig(level=default_level)
        coloredlogs.install(level=default_level)
        print("Failed to find logging configuration file {}, using defaults".format(config_file))
