import logging
from .constants import STOPWATCH_LOGGER
import time

"""Purpose: General logger. Attach run id to all log statements issued by the adapter"""


class LoggerAdapter(logging.LoggerAdapter):
    def __init__(self, logger, run_id):
        super(LoggerAdapter, self).__init__(logger, {'run_id': run_id})


"""Purpose: Used by stopwatch logger to time tasks"""


class Timer:
    def __init__(self, run_id):
        self.start_time = -1
        self.stop_time = -1
        self.logInput = {'time_passed': -1, 'run_id': run_id}

    def start(self):
        self.start_time = time.time()*1000

    def stop(self):
        self.stop_time = time.time()*1000
        self.logInput['time_passed'] = round(self.stop_time - self.start_time)


stopwatch_logger = logging.getLogger(STOPWATCH_LOGGER)


class StopWatchAdapter(logging.LoggerAdapter):
    def __init__(self, run_id):
        self.timer = Timer(run_id)
        super(StopWatchAdapter, self).__init__(stopwatch_logger, self.timer.logInput)

    def start(self):
        self.timer.start()

    def stop(self):
        self.timer.stop()
