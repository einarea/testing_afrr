# Module dataObjects.py
# Module that defines objects holding data related to the bid selection model
# It also contains a class to store solution information

from .afrr_errors import ImplementationError, InitializationError, InputError
from .constants import *
from .basemodel import Variable
from .utilities import DataVector, DictHelper, iterator, callback
from collections import Iterable
import inspect
from copy import deepcopy


''' Shared timeholder object. The timeholder object should be a singleton and shared between all
area and bidset instances. The timeholder class includes the number of hours, an iterable hour range
and a method to create a hourrange of any object,'''


class TimeHolder:
    def __init__(self, n_hours=1):
        self._nhours = None
        self.nhours = n_hours
        self._hour_range = [i for i in range(1, self.nhours + 1)]

    @property
    def nhours(self):
        return self._nhours

    def hour_range(self, obj_type, *args):
        hour_range = HourRange(self._hour_range)
        return hour_range.get_hour_range(obj_type, *args)

    @nhours.setter
    def nhours(self, n_hours):
        if type(n_hours) is not int:
            raise InitializationError('n_hours is not integer')
        self._nhours = n_hours

    @property
    def hours(self):
        return self._hour_range

    def __eq__(self, other):
        if type(other) is not TimeHolder:
            return False
        if self.nhours != other.nhours:
            return False
        return True

    def __neg__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return str(self._hour_range)


''' Class to create time dependent fields. A hourrange is an iterable of any object. The object
may be instantiated with arguments. To avoid having to index hours off by 1, the class overloads _getitem_. 
A hour range should only be created through the shared timeholder object to ensure time consistency.'''


class HourRange:
    primitives = [int, float, bool, str, list, dict]
    numbers = []

    def __init__(self, hour_range):
        self.Hours = None
        self.hour_range = hour_range
        self.type = None

    def get_hour_range(self, obj_type, *args):
        if type(obj_type) in HourRange.primitives:
            self.type = obj_type
            self.Hours = [deepcopy(obj_type) for i in self.hour_range]
        elif inspect.isclass(obj_type) > 0:
            try:
                self.Hours = [obj_type(*args) for i in self.hour_range]
            except TypeError as e:
                raise InitializationError('Object is not callable' + str(e))
        else:
            self.Hours = [deepcopy(obj_type) for i in self.hour_range]
        return self

    def __getitem__(self, item):
        return self.Hours[item - 1]

    def __setitem__(self, key, value):
        self.Hours[key - 1] = value

    def __str__(self):
        _str = ''
        for element in self.Hours:
            _str += str(element)+'\n'
        return _str

    def __len__(self):
        return len(self.hour_range)


''' Superclass of tso, macroarea and region. The class is not meant to be directly instantiated.
 Contains in- and out connections to the area as well as an area id as a common attribute. 
 All area instances may contain bidsets. Also all area instances must be initialized with a time_holder
 object. The time_holder should be shared between all area instances for consistency.'''


class Area:
    # Default hour range
    def __init__(self, _id, time_holder, direction_holder, prod_min_penalty=0):
        self.OutEdges = DataVector()  # Structures to store connections to other macro areas.
        self.InEdges = DataVector()  # A connection is defined by class Edge
        self.Connections = DataVector()
        self._prod_min_penalty = 0
        self.prod_min_penalty = prod_min_penalty
        self._id = 'None'
        self.Cleared = False
        self.id = _id
        self._time_holder = None
        self.time_holder = time_holder
        self._direction_holder = direction_holder
        self.BidSets = DictHelper(BidSet, self.time_holder)
        self.prod_min = self.time_holder.hour_range(Direction, self.direction_holder)
        self.prod_min_var = self.time_holder.hour_range(Direction, self.direction_holder)
        self.prod_max = self._init_prod_max()
        self._init_direction_variable(self.prod_min_var)

    @property
    def direction_holder(self):
        return self._direction_holder

    @direction_holder.setter
    def direction_holder(self, direction_holder):
        if type(direction_holder) is not DirectionHolder:
            raise InitializationError('Direction holder is not of type DirectionHolder')
        self._direction_holder = DirectionHolder

    def get_connection(self, to_area):
        return self.OutEdges.filter_object(lambda connection: connection.to_area == to_area)

    def set_prod_max(self, value, hour, direction):
        if type(hour) is int and hour in self.hours:
            if type(value) is int or type(value) is float:
                self.prod_max[hour].set_direction_value(value, direction)
                return
        raise InitializationError('Cannot set prod min for area: ' + str(self.id) +
                                  ', value is not integer or hour is outside time range')

    def set_prod_min(self, value, hour, direction):
        if type(hour) is int and hour in self.hours:
            if type(value) is int or type(value) is float:
                self.prod_min[hour].set_direction_value(value, direction)
                return
        raise InitializationError('Cannot set prod min for area: '+str(self.id) +
                                  ', value is not integer or hour is outside time range')

    def _init_direction_variable(self, holder):
        for hour in self.hours:
            holder[hour].up = Variable()
            holder[hour].down = Variable()

    def _init_prod_max(self):
        prod_max = self.time_holder.hour_range(Direction, self.direction_holder)
        for hour in self.hours:
            prod_max[hour].up = None
            prod_max[hour].down = None
        return prod_max

    @property
    def prod_min_penalty(self):
        return self._prod_min_penalty

    @prod_min_penalty.setter
    def prod_min_penalty(self, penalty):
        if type(penalty) is not int and type(penalty) is not float:
            raise InitializationError('Penalty is not integer or float')
        self._prod_min_penalty = penalty

    @property
    def directions(self):
        return self.direction_holder.directions

    @property
    def up_code(self):
        return self.direction_holder.up_code

    @property
    def down_code(self):
        return self.direction_holder.down_code

    @property
    def hours(self):
        return self.time_holder.hours

    @property
    def time_holder(self):
        return self._time_holder

    @time_holder.setter
    def time_holder(self, time_holder):
        if not type(time_holder) is TimeHolder:
            raise InitializationError('Timeholder is not of type time holder')
        self._time_holder = time_holder

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    # Indicate if the area is cleared, i.e bids are selected and demand fulfilled
    def set_cleared(self, indicator):
        self.Cleared = indicator

    def get_in_capacity(self, hour):
        return self.InEdges.getSum(lambda con: con.getCapacity(hour=hour))

    def is_cleared(self):
        return self.Cleared

    def _get_bidset(self, key):
        if key in self.BidSets.keys:
            return self.BidSets[key]
        else:
            return BidSet(self.time_holder)

    @property
    def up_bids(self):
        return self._get_bidset(self.direction_holder.up_code)

    @up_bids.setter
    def up_bids(self, bids):
        self.up_bids.add_bid(bids)

    @property
    def down_bids(self):
        return self._get_bidset(self.direction_holder.down_code)

    @down_bids.setter
    def down_bids(self, bids):
        self.down_bids.add_bid(bids)

    @property
    def accepted_bids(self):
        return self.all_bids.filter(lambda bid: bid.status == BID_SELECTED)

    @property
    def all_bids(self):
        return BidSet(self.time_holder,
                      DataVector.union([self.down_bids, self.up_bids]))

    def remove_bidsets(self):
        self.BidSets = {}

    def __eq__(self, other):
        if self.id == other.id:
            return True


class NordicArea(Area):
    def __init__(self, time_holder, direction_holder, regions, macro_areas, instance_id=None):
        super(NordicArea, self).__init__('NordicArea', time_holder, direction_holder)
        self.instance_id = instance_id
        self._regions = DataVector()
        self.regions = regions
        self._macro_areas = DataVector()
        self.macro_areas = macro_areas
        self.linked_bids = DataVector()
        self.exclusive_bids = DataVector()
        self.demand = self.time_holder.hour_range(Direction, self.direction_holder)
        self.init_demand_functions()

    @property
    def connection_info(self):
        info_holder = []
        for connection in self.connections:
            for hour in self.hours:
                if connection[hour].dir_flow.up.solution_val >= ZERO_TOLERANCE:
                    info_holder.append({
                        HOUR: hour,
                        FROM_REGION: connection.from_area.id,
                        TO_REGION: connection.to_area.id,
                        DIRECTION: self.direction_holder.up,
                        RESERVERD_CAP: round(connection[hour].dir_flow.up.solution_val,0)
                    })
                if connection[hour].dir_flow.down.solution_val >= ZERO_TOLERANCE:
                    info_holder.append({
                        HOUR: hour,
                        FROM_REGION: connection.from_area.id,
                        TO_REGION: connection.to_area.id,
                        DIRECTION: self.direction_holder.down,
                        RESERVERD_CAP: round(connection[hour].dir_flow.down.solution_val,0)
                    })
        return info_holder

    @property
    def total_cost(self):
        bid_cost = self.accepted_bids.getSum(lambda bid: bid.price*bid.volume_sol *
                                             (bid.hour_to-bid.hour_from))
        flow_cost = self.connections.getSum(lambda connection: sum([connection[h].volume_sol *
                                                                    connection[h].cost for h in self.hours]))
        return bid_cost+flow_cost

    def init_demand_functions(self):
        for hour in self.hours:
            self.demand[hour].up = callback(lambda _hour: self.regions.getSum(lambda region: region.demand[_hour].up),
                                            hour)
            self.demand[hour].down = callback(lambda _hour: self.regions.getSum(lambda region:
                                                                                region.demand[_hour].down), hour)

    def __str__(self):
        _str = str(self.id)
        _str += '\nRegions\n'
        for region in self.regions:
            _str += str(region.id)+'\n'

        _str += '\nMacro areas\n'
        for area in self.macro_areas:
            _str += str(area.id) + '\n'

        _str += '\n Bid information and total demand \n'
        _str += 'Hour\tn upbids\tUP volume\tUP vol_sol\tUP demand\tn downbids\tDOWN volume\tDOWN vol_sol\tDOWN demand\n'
        up_bids = self.up_bids
        down_bids = self.down_bids
        for hour in self.hours:
            _str += (str(hour) + '\t\t%5d' % len(up_bids[hour]) + '\t\t%5d' %
                     up_bids[hour].getSum(lambda bid: bid.volume) + '\t\t%5d' %
                     up_bids[hour].getSum(lambda bid: bid.volume_sol) +
                     '\t\t%5d' % self.get_up_demand(hour) + '\t\t%5d' % len(down_bids[hour]) +
                     '\t\t%5d' % down_bids[hour].getSum(lambda bid: bid.volume) + '\t\t%5d' %
                     down_bids[hour].getSum(lambda bid: bid.volume_sol) +
                     '\t\t%5d' % self.get_down_demand(hour)+'\n')

        _str += '\nLinked bid groups: ' + str(len(self.linked_bids)) + '\n'
        _str += 'Exclusive bid groups: ' + str(len(self.exclusive_bids)) + '\n'
        return _str

    def get_up_demand(self, hour):
        return self.regions.getSum(lambda region: region.demand[hour].up)

    def get_down_demand(self, hour):
        return self.regions.getSum(lambda region: region.demand[hour].down)

    @property
    def connections(self):
        connections = DataVector()
        for region in self.regions:
            for edge in region.OutEdges:
                connections.add_object(edge)
        return connections

    @property
    def macro_areas(self):
        return self._macro_areas

    @macro_areas.setter
    def macro_areas(self, macro_area):
        macro_area = iterator(macro_area)
        for area in macro_area:
            if type(area) is not MacroArea:
                raise InitializationError('area is not of type macroarea')
            self.macro_areas.add_object(area)

    @property
    def regions(self):
        return self._regions

    @regions.setter
    def regions(self, regions):
        for reg in regions:
            if type(reg) is not Region:
                raise InitializationError('Region is not of type region')
            self._regions.add_object(reg)

    def add_linked_bids(self, bids):
        self.linked_bids.add_object(DataVector(bids), flattening=False)
        for bid1 in bids:
            for bid2 in bids:
                if bid1 != bid2:
                    bid1.add_linked_bid(bid2)

    def add_exclusive_bids(self, bids):
        self.exclusive_bids.add_object(DataVector(bids), flattening=False)
        self.set_exclusive_bid_coefficients(bids)

    @staticmethod
    def set_exclusive_bid_coefficients(exclusive_bids):
        for bid in exclusive_bids:
            linked_bids = bid.linked_bids
            if len(linked_bids) >= 1:
                for linked_bid in linked_bids:
                    if linked_bid in exclusive_bids:
                        bid.exclusive_bid_coefficient = 0.5

    @property
    def all_bids(self):
        return BidSet(self.time_holder,
                      DataVector.union([self.up_bids, self.down_bids]))

    @property
    def up_bids(self):
        up_bids = BidSet(self.time_holder)
        for reg in self.regions:
            up_bids.add_bid(reg.up_bids)
        return up_bids

    @property
    def down_bids(self):
        down_bids = BidSet(self.time_holder)
        for reg in self.regions:
            down_bids.add_bid(reg.down_bids)
        return down_bids


''' Class to store a block of bids. Contains bids in the block, the hour range of the block
and variable fields. The class provides block validation, i.e. bids must 
be in consecutive hours. '''


class Block:
    i = 0

    def __init__(self, bids):
        self.acceptBlock = Variable()
        self.blockId = Block.i
        Block.i += 1
        self._bids = DataVector()
        self.hour_from = -1
        self.hour_to = -1
        self._region = None
        self.acceptBlock = Variable()
        self.bids = bids

    @property
    def region(self):
        return self._region

    @region.setter
    def region(self, reg):
        self._region = reg

    @property
    def block_length(self):
        return len(self._bids)

    def __eq__(self, other):
        return self.blockId == other.blockId

    def __getitem__(self, item):
        return self.bids[item]

    @property
    def bids(self):
        return self._bids

    @bids.setter
    def bids(self, bids):
        # assert bid type
        if not isinstance(bids, Iterable):
            bids = [bids]
        for bid in bids:
            if not isinstance(bid, Bid):
                raise Exception('Block is not initialized with bid')
        bids = sorted(bids, key=lambda _bid: _bid.hour)
        # assert bids are in consecutive hours
        hour_prev = bids[0].hour
        first = True
        for bid in bids:
            if not first:
                hour = bid.hour
                if not hour == hour_prev + 1:
                    raise Exception('Block is not initialized with consecutive bids')
                hour_prev += 1
            else:
                first = False

        self.hour_from = bids[0].hour
        self.region = bids[0].region
        self.region.blocks = self
        self.hour_to = bids[-1].hour
        self.bids.add_object(bids)
        for bid in bids:
            bid.blocks = self

    @property
    def name(self):
        return 'block_' + str(self.blockId) + '_hfr_' + str(self.hour_from) + '_hto_' + str(
            self.hour_to) + '_reg_' + str(self.region.id)

    def is_in_macro_area(self, areas):
        for area in areas:
            if self.bids[0].is_in_macro_area(area):
                return True
        return False


class DirectionHolder:
    def __init__(self, up, down, up_code, down_code):
        self.down_code = down_code
        self.up_code = up_code
        if type(up) is not int:
            raise InitializationError('up code is not integer')

        if type(down) is not int:
            raise InitializationError('down code is not integer')

        if type(up_code) is not str:
            raise InitializationError('down key is not integer')

        if type(down_code) is not str:
            raise InitializationError('up key is not integer')

        self.directions = {up_code: up, down_code: down}

    @property
    def up(self):
        return self.directions[self.up_code]

    @property
    def down(self):
        return self.directions[self.down_code]

    def direction(self):
        return Direction(self)

    def get_code(self, direction):
        if direction == self.up:
            return self.up_code
        elif direction == self.down:
            return self.down_code
        else:
            raise ImplementationError('Wrong direction code')

    def __eq__(self, other):
        if (self.up == other.up and self.down == other.down and self.down_code == other.down_code and
                self.up_code == other.up_code):
            return True
        return False

    def __neg__(self, other):
        return not self.__eq__(other)


class Direction:
    def __init__(self, direction_holder):
        self.up = 0
        self.down = 0
        self.direction_holder = direction_holder

    def set_direction_value(self, value, direction):
        if direction not in self.direction_holder.directions.values():
            raise InitializationError('Direction is not part of directions in direction object')
        if direction == self.direction_holder.down:
            self.down = value
        else:
            self.up = value

    @property
    def up_code(self):
        return self.direction_holder.up_code

    @property
    def down_code(self):
        return self.direction_holder.down_code


''' Class that represents a region. Main purpose is to store bid and blocks in the region. '''


class Region(Area):
    def __init__(self, _id, time_holder, direction_holder, prod_min_penalty=0, demand_penalty=0):
        super(Region, self).__init__(_id, time_holder, direction_holder, prod_min_penalty)
        self._demand_penalty = 0
        self.demand_penalty = demand_penalty
        self._macro_area = DataVector()
        self._blocks = DataVector()
        self.time_holder = time_holder
        self.demand = self.time_holder.hour_range(Direction, self.direction_holder)
        self.demand_penalty_var = self.time_holder.hour_range(Direction, self.direction_holder)
        self._init_direction_variable(self.demand_penalty_var)

    @property
    def demand_penalty(self):
        return self._demand_penalty

    @demand_penalty.setter
    def demand_penalty(self, demand_penalty):
        if type(demand_penalty) is not int and type(demand_penalty) is not float:
            raise InitializationError('Demand penalty in bidding xone is not integer or floet')
        self._demand_penalty = demand_penalty

    def get_demand(self, hour):
        return self.demand[hour]

    def set_demand(self, hour, value, direction):
        if type(hour) is not int or hour not in self.time_holder.hours:
            raise InitializationError('Hour for demand in ' + str(self.id) + ' is outside time range of problem')
        if type(value) is int or type(value) is float:
            self.demand[hour].set_direction_value(value, direction)
        else:
            raise InitializationError('Demand is not integer or float')

    def __str__(self):
        _str = ''
        _str += 'Region ' + str(self.id) + '\n'
        _str += 'Demand and bid volumes\n'
        up_bids = self.up_bids
        down_bids = self.down_bids
        _str += ('Hour\tUP demand\tUp bid vol\tUP vol sol\tDOWN dem\tDOWN bid\tDOWN vol sol\tMin UP\t'
                 'Min DOWN\tMax UP\tMax DOWN \n')

        for hour in self.hours:
            _str += (str(hour) + '\t\t'
                     + '%5d' % self.demand[hour].up + '\t\t' +
                     '%5d' % up_bids[hour].getSum(lambda bid: bid.volume) + '\t\t' +
                     '%5d' % up_bids[hour].filter(lambda bid: bid.is_cleared()).getSum(lambda _bid: _bid.volume_sol)
                     + '\t\t' + '%5d' % self.demand[hour].down +
                     '\t\t' + '%5d' % down_bids[hour].getSum(lambda bid: bid.volume) + '\t\t'
                     + '%5d' % down_bids[hour].filter(lambda bid: bid.is_cleared()).getSum(lambda bid: bid.volume_sol)
                     + '\t\t' + '%5d' % self.prod_min[hour].up + '\t\t%5d' % self.prod_min[hour].down + '\t\t'
                     '%5s' % self.prod_max[hour].up + '\t\t%5s' % self.prod_max[hour].down + '\n')

        _str += '\nBid information\n'
        _str += 'n_upbids: ' + str(len(self.up_bids)) + ' n_downbids: ' + str(len(self.down_bids)) + '\n'
        return _str

    @property
    def macro_area(self):
        if len(self._macro_area) == 1:
            return self._macro_area[0]
        else:
            return self._macro_area

    @property
    def blocks(self):
        return self._blocks

    @blocks.setter
    def blocks(self, blocks):
        self._blocks.add_object(blocks)

    @macro_area.setter
    def macro_area(self, area):
        self._macro_area.add_object(area)

    def add_macro_area(self, area):
        if type(area) is not MacroArea:
            raise Exception('Area must be of class MacroArea')
        self._macro_area.add_object(area)

    def is_in_macro_area(self, macro_area):
        for area in self._macro_area:
            if area.id == macro_area.id:
                return True
        return False

    def has_connection(self, to_area):
        if self.get_connection(to_area):
            return True
        return False


''' Class that represents a macroarea containing one or several regions.
Stores information such as demand for a given hour and connections to other macro areas '''


class MacroArea(Area):
    def __init__(self, _id, time_holder, direction_holder, prod_penalty=0):
        super(MacroArea, self).__init__(_id, time_holder, direction_holder, prod_penalty)
        # Set to true if demand is not to be included in energy balance constraint
        self._regions = DataVector()
        self.marginal_price = self.time_holder.hour_range(Variable)
        self.export_price = self.time_holder.hour_range(float(0))

    def __str__(self):
        _str = ''
        _str += 'Macro Area: '+str(self.id)

        _str += '\nRegions\n'
        for region in self.regions:
            _str += str(region.id) + '\n'

        _str += '\nProd min\n'
        for hour in self.hours:
            _str += ('Hour ' + str(hour) + ': ' + str(self.up_code) + ': ' +
                     str(self.prod_min[hour].up) + ' ' +
                     str(self.down_code)+': ' + str(self.prod_min[hour].down)+'\n')

        _str += '\nProd max\n'
        for hour in self.hours:
            _str += ('Hour ' + str(hour) + ': ' + str(self.up_code) + ': ' +
                     str(self.prod_max[hour].up) + ' ' +
                     str(self.down_code) + ': ' + str(self.prod_max[hour].down)+'\n')

        return _str

    def get_import(self, hour):
        if hour in self.hours:
            return self.InEdges.getSum(lambda connection: connection.get_flow(hour))
        return 0

    def get_export(self, hour):
        if hour in self.hours:
            return self.OutEdges.getSum(lambda connection: connection.get_flow(hour))
        return 0

    @property
    def connections(self):
        return DataVector.union([self.OutEdges, self.InEdges])

    @property
    def up_bids(self):
        return BidSet(self.time_holder, DataVector([bid for reg in self.regions for bid in reg.up_bids]))

    @property
    def down_bids(self):
        return BidSet(self.time_holder, DataVector([bid for reg in self.regions for bid in reg.down_bids]))

    @property
    def regions(self):
        return self._regions

    @regions.setter
    def regions(self, regions):
        if not isinstance(regions, Iterable):
            regions = [regions]
        for region in regions:
            if not type(region) is Region:
                raise InputError('Region needs to be of class Region')
            self._regions.add_object(region)
            region.add_macro_area(self)


"""Class to store bid information and bid variables. Also provides several fields to identify bid states
and bid statuses. The class has initialization validation for the data fields and raises 
input error for any malicious data."""


class Bid:
    statuses = [BID_UNSELECTED, BID_SELECTED, BID_SKIPPED]

    def __init__(self, _id, direction, region, hour_from, hour_to, volume, price, min_vol=None, quantity_factor=1):
        self._region = None
        self._quantity_factor = 1
        self.quantity_factor = quantity_factor
        self.exclusive_bid_coefficient = 1
        self.region = region
        self._direction = None
        self._min_vol = None
        self.linked_bids = DataVector()
        self.direction = direction
        self.id = _id
        self._price = None
        self._hour_from = 0
        self._hour_to = 0
        self.hour_from = hour_from
        self.hour_to = hour_to
        self._blocks = DataVector()
        self._volume = None
        self._status = BID_UNSELECTED
        self.accept_bid_var = Variable()
        self.volume_var = Variable()
        self.volume_sol = 0
        self.cleared = False
        self._available = True
        self._valid = True
        self.price = price
        self.volume = volume
        self.min_vol = min_vol
        self.region.BidSets[self.direction_code].add_bid(self)

    def add_linked_bid(self, bid):
        self.linked_bids.add_object(bid)

    @property
    def quantity_factor(self):
        return self._quantity_factor

    @quantity_factor.setter
    def quantity_factor(self, factor):
        if type(factor) is not int and type(factor) is not float:
            raise InitializationError('Min volume is not integer')
        self._quantity_factor = factor

    @property
    def direction_code(self):
        return self.region.direction_holder.get_code(self.direction)

    @property
    def min_vol(self):
        return self._min_vol

    @min_vol.setter
    def min_vol(self, value):
        if value is None:
            value = self.volume
        if type(value) is not int and type(value) is not float:
            raise InitializationError('Min volume is not integer')
        self._min_vol = value

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, direction):
        if direction not in self.region.directions.values():
            raise InitializationError('Bid direction is not a direction in direction object')
        self._direction = direction

    def delete_bid(self):
        self.region.BidSets[self.type].remove_bid(self)

    def __str__(self):
        return ('Bid: ' + str(self.name) + ', hour_from: ' + str(self.hour_from) + ', hour_to: '+str(self.hour_to) +
                ', direction: '+str(self.direction_code) + ', volume: ' +
                str(self.volume) + ', volume_sol: ' + str(self.volume_sol) + ' price: ' + str(self.price) +
                ' quantity factor: ' + str(self.quantity_factor)+'\n')

    # property method used to decide if bid should be part of optimization
    @property
    def available(self):
        return not self.is_cleared() and self._available and self._valid

    @available.setter
    def available(self, is_available):
        self._available = is_available

    @property
    def type(self):
        return self.direction

    @property
    def hour_from(self):
        return self._hour_from

    @hour_from.setter
    def hour_from(self, hour):
        if type(hour) is int and hour in self.region.hours:
            self._hour_from = hour
        else:
            raise InitializationError('Hour is not integer' + str(hour))

    @property
    def hour_to(self):
        return self._hour_to

    @hour_to.setter
    def hour_to(self, hour):
        if type(hour) is int and (hour in self.region.hours or hour == self.region.hours[-1]+1):
            self._hour_to = hour
        else:
            raise InitializationError('Hour is not integer' + str(hour))

    @property
    def macro_area(self):
        return self.region.macro_area

    @property
    def name(self):
        return 'Bid_' + str(self.id) + '_' + str(self.direction_code)+'_hf_' + str(self.hour_from) + '_ht_' + \
               str(self.hour_to)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if status not in Bid.statuses:
            raise ImplementationError('Attempted to set illegal bid status')
        self._status = status
        if self._status == BID_SELECTED:
            self.set_cleared(True)

    @property
    def blocks(self):
        return self._blocks

    @blocks.setter
    def blocks(self, block):
        self.blocks.add_object(block)

    def in_hour(self, hour):
        if hour == self.hour_from:
            return True
        else:
            return False

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, price):
        if type(price) is int or type(price) is float:
            self._price = price
        else:
            raise InitializationError('Price is not integer or float for bid: ' + str(self.id))

    @property
    def volume(self):
        return self._volume

    @volume.setter
    def volume(self, volume):
        if type(volume) is int or type(volume) is float:
            self._volume = volume
        else:
            raise InitializationError('Volume is not integer or float for bid: ' + str(self.id))

    def set_cleared(self, ind):
        self.cleared = ind
        if ind:
            self._status = BID_SELECTED
        else:
            self._status = BID_UNSELECTED
            self.volume_sol = 0

    def is_cleared(self):
        return self.cleared

    def is_in_macro_area(self, macro_areas):
        for area in macro_areas:
            if self.region.isInMacroArea(area):
                return True
        return False

    def is_in_tso(self, tsos):
        for tso in tsos:
            if tso == self.region.macro_area.tso:
                return True
        return False

    @property
    def region(self):
        return self._region

    @region.setter
    def region(self, region):
        if type(region) is not Region:
            raise InitializationError('Regions is not of class region in bid')
        self._region = region

    def __eq__(self, other):
        if self.id == other.id:
            return True
        return False


"""Helper class to separate bids in different sets and distribute them between hours.
Also acts as validation for bid hour and issues warning if bid is not within the defined 
problem time range"""


class BidSet(DataVector):
    def __init__(self, time_holder, bids=DataVector()):
        super(BidSet, self).__init__()
        # Structure to distribute bids to hours
        self._hours = None
        self.time_holder = time_holder
        self.hours = self.time_holder.hour_range(DataVector)
        # Initialize bid values for all hours included. A particular bid value is defined by class Bid
        self.add_bid(bids)

    def __str__(self):
        string_holder = ''
        for bid in self:
            string_holder += str(bid)

        return string_holder

    def remove_bid(self, bid):
        self[bid.hour_from].remove_object(bid)
        for i in range(0, len(self.elements)):
            if bid.name == self.elements[i].name:
                del self.elements[i]
                break

    @property
    def hours(self):
        return self._hours

    @property
    def volume(self):
        return self.getSum(lambda bid: bid.volume)

    @property
    def volume_sol(self):
        return self.getSum(lambda bid: bid.volume_sol)

    @hours.setter
    # TODO add control
    def hours(self, hours):
        self._hours = hours

    def __len__(self):
        return len(self.elements)

    def filter(self, bool_func):
        return BidSet(self.time_holder, super(BidSet, self).filter(bool_func))

    def sub_bid_set(self, func):
        return BidSet(self.time_holder, self.filter(func))

    @property
    def cost(self):
        return self.getSum(lambda bid: bid.price * bid.VolumeSol)

    def to_dict(self):
        info_holder = []
        for bid in self.sort(lambda _bid: _bid.id):
            info_holder.append({ID: bid.id, ACCEPTED_VOLUME: bid.volume_sol})
        return info_holder

    def print_set(self):
        for bid in self.to_data_frame():
            print(bid)

    def to_data_frame(self):
        bid_holder = []
        for bid in self:
            bid_holder.append({
                'BidID': bid.id,
                'Volume': bid.volume,
                'VolumeSol': bid.volume_sol,
                'Region': bid.region.id,
                'Hour': bid.hour,
                'Price': bid.price
            })

        return bid_holder

    def get_bids(self, func):
        return DataVector([bid for bid in self if func(bid)])

    def __getitem__(self, hour):
        return self.hours[hour]

    def __setitem__(self, hour, value):
        self.hours[hour] = value

    def get_bid(self, _id):
        return self.get_object(_id)

    def in_bid_set(self, bid):
        self.inVector(bid)

    def add_bid(self, bids):
        def _control(_bid):
            if all([_bid.hour_from >= 1, _bid.hour_to <= len(self.hours)+1, type(_bid) is Bid]):
                return True
            else:
                return False

        if isinstance(bids, Iterable):
            for bid in bids:
                if _control(bid):
                    for hour in range(bid.hour_from, bid.hour_to):
                        self[hour].add_object(bid)
                        if hour == bid.hour_from:
                            self.add_object(bid)
        else:
            if _control(bids):
                for hour in range(bids.hour_from, bids.hour_to):
                    self[hour].add_object(bids)
                    if hour == bids.hour_from:
                        self.add_object(bids)


"""Represents flow on connection in a particular hour. Store volume variable and cost of flow. May store
 capacity if capacity is time varying."""


class Flow:
    def __init__(self, connection):
        self._capacity = 0
        self._connection = None
        self._cost = 0
        self.cost = connection.cost
        self.volume_sol = 0 
        self.connection = connection
        self.capacity = connection.capacity
        self.agg_volume = Variable()
        self.dir_flow = Direction(self.connection.from_area.direction_holder)
        self.dir_flow.up = Variable()
        self.dir_flow.down = Variable()

    def __str__(self):
        return str(self.capacity) + ' flow: ' + str(self.agg_volume.solution_val) + ' cost: ' + str(self.cost)

    @property
    def up(self):
        return self.dir_flow.up

    @property
    def down(self):
        return self.dir_flow.down

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, connection):
        if not isinstance(connection, Connection):
            raise ImplementationError('Connection is not of type Connection')
        self._connection = connection

    @property
    def capacity(self):
        return self._capacity

    @capacity.setter
    def capacity(self, capacity):
        if type(capacity) is not int and type(capacity) is not float:
            raise InitializationError('Capacity is not int or float')
        self._capacity = capacity

    @property
    def cost(self):
        return self._cost

    @cost.setter
    def cost(self, cost):
        if type(cost) is not int and type(cost) is not float:
            raise InitializationError('Capacity is not int or float')
        self._cost = cost


"""Represents connection between two areas. Store connection static information such as capacity and
variable informaton such as flow"""


class Connection:
    def __init__(self, from_area, to_area, capacity=0, cost=0):
        self._cost = 0
        self.cost = cost
        self._capacity = 0
        self._from_area = None
        self._to_area = None
        self.to_area = to_area
        self.from_area = from_area
        from_area.OutEdges.add_object(self)
        self.capacity = capacity
        self.flow = self.from_area.time_holder.hour_range(Flow, self)
        to_area.InEdges.add_object(self)

    def __str__(self):
        _str = '\nFrom: ' + str(self.from_area.id) + ' to: ' + str(self.to_area.id) + '\n'
        for hour in self._from_area.hours:
            _str += ('Hour: ' + str(hour) + ' capacity: '+str(self[hour])+'\n')
        return _str

    def get_flow(self, hour):
        return self[hour].volume.solution_val

    @property
    def cost(self):
        return self._cost

    @cost.setter
    def cost(self, cost):
        if type(cost) is not int and type(cost) is not float:
            raise InitializationError('Cost is not integer or float')
        self._cost = cost

    def set_cost(self, hour, cost):
        if hour not in self.from_area.hours:
            raise InitializationError('Hour is outside time range of problem')
        self[hour].cost = cost

    def set_capacity(self, hour, capacity):
        if hour not in self.from_area.hours:
            raise InitializationError('Hour is outside time range of problem')
        self[hour].capacity = capacity

    @property
    def from_area(self):
        return self._from_area

    @from_area.setter
    def from_area(self, from_area):
        if type(from_area) is not Region:
            raise InitializationError('From area is not of type macroarea')
        self._from_area = from_area

    @property
    def to_area(self):
        return self._to_area

    @to_area.setter
    def to_area(self, to_area):
        if type(to_area) is not Region:
            raise InitializationError('To area is not of type macroarea')
        self._to_area = to_area

    @property
    def name(self):
        return 'from_' + str(self.from_area.id) + '_to_' + str(self.to_area.id)

    def create_flow_variables(self):
        [flow.create_flow_variable() for flow in self]

    @property
    def capacity(self):
        return self._capacity

    @capacity.setter
    def capacity(self, capacity):
        if type(capacity) is not int and type(capacity) is not float:
            raise InitializationError('Capacity is not integer or float')
        self._capacity = capacity

    def get_tso(self):
        self.from_area.getTSO()

    @property
    def hours(self):
        return self.from_area.hours

    def getformat(self):
        output = []
        for flow in self.flow:
            output.append({
                'from': self.from_area.Id,
                'to': self.to_area.Id,
                'hour': flow.Hour,
                'flow': flow.VolumeVal
            })
        return output

    def is_within_country(self):
        return self.from_area.tso == self.to_area.tso

    def get_var(self, hour):
        return self[hour].getVar()

    def __getitem__(self, item):
        return self.flow[item]

    def __eq__(self, other):
        if self.from_area.id == self.from_area.id and self.to_area == self.from_area:
            return True
        else:
            return False
