from .loggers import LoggerAdapter
import logging
import xpress as xp
import time
from .utilities import DataVector
from .afrr_errors import XpressError, LicenseError, ImplementationError

# Global xpress constants
ERROR_CODE = 'errorcode'
RANDOM_SEED_CONTROL = 'prepermuteseed'

logger = logging.getLogger(__name__)

"""Purpose: Integer solution callback during the solution search. Register solution on solution object"""


def integer_sol_callback(prob, solution):
    solution.status = Solution.SOL_FOUND
    solution.gap_set(lb=prob.attributes.bestbound, ub=prob.attributes.mipobjval)


"""Purpose: Interupt model if stop signal is registered on the model"""


def stop_callback(prob, model, msg, len, msgtype):
    if model.stop_solve:
        model.logger.info('About to interupt')
        prob.interrupt(xp.stop_user)
        model.interrupted = True
        model.logger.info('Model interrupted')


""" Purpose: Closure to capture the initial time, gap, increment and interval """


def add_new_node_callback(time_val, gap, interval, increment):
    """ Purpose: New_node_callback increases the stopcriterion for optimalitygap. It is called in each BB node.
        Input: The signature is required by xpress. See xpress documentation for addcbnode callback"""
    def new_node_callback(prob, _logger, parentnode, newnode, branch):
        nonlocal time_val, gap
        if interval < time.time() - time_val:
            _logger.info("Increased gap from " + str(gap) + " to " + str(gap + increment))
            prob.setControl('miprelstop', gap + increment)
            time_val = time.time()
            gap = gap + increment
    return new_node_callback


''' Purpose: Log output from xpress solver. Called every time xpress pushes a messages '''


def logger_callback(prob, _logger, msg, len, msgtype):
    _logger.info(msg)


''' Purpose: General base optimization model. Meant to be specialized as a derived model. Wraps all xpress interaction. 
Store variables and rows. Register them on an xpress problem and solves. The class sets up xpress callbacks and
xpress controls. Users may register solution handlers and infeasibility handlers on the model'''


class Model:
    def __init__(self, name="model", solution=None, random_seed=0, initial_gap=0, gap_interval=1000, gap_increment=0,
                 max_time=None, run_id=None):

        self.problem = xp.problem(name=name)
        self.stop_solve = False
        self.logger = LoggerAdapter(logger, run_id)
        self.name = name
        self.problem.addcbmessage(logger_callback, self.logger, 0)
        self.variables = DataVector()
        self.Objective_Elements = []
        self.random_seed = random_seed
        self.constraints = DataVector()
        self.solution_listener = []
        self.handle_infeasibility = []
        if solution is None:
            self.solution = Solution()
        else:
            if isinstance(solution, Solution):
                self.solution = solution
            else:
                raise ImplementationError('Solution object provided is not of type Solution')
        self.solution.name = name
        self.initial_gap = initial_gap
        self.gap_interval = gap_interval
        self.gap_increment = gap_increment
        self.max_time = max_time

    def clear_model(self):
        self.constraints = DataVector()

    def add_infeasibility_listener(self, func):
        self.handle_infeasibility.append(func)

    def add_solution_listener(self, func):
        self.solution_listener.append(func)

    """Purpose: Register variables in variable holder"""
    def add_variable(self, *variables):
        for variable in variables:
            if type(variable) is not Variable:
                raise ImplementationError('variable added is not of type Variable')
            self.variables.add_object(variable)

    """Purpose: Add variables in variable holder to model"""
    def add_variables(self):
        self.logger.info('Adding variables to xpress problem')
        not_added_var = self.variables.filter(lambda var: not var.added)

        try:
            self.problem.addVariable([variable.get_var() for variable in not_added_var])
            for variable in not_added_var:
                variable.added = True
        except Exception as XpressException:
            self.error_handling(XpressException)

    """Purpose: Register constraint in constraint holder"""
    def create_constraint(self, ctr):
        self.constraints.add_object(ctr)

    """Purpose: Add constraints in constraint holder to model"""
    def add_constraints(self):
        try:
            for ctr in self.constraints:
                self.problem.addConstraint(ctr)
        except Exception as XpressException:
            self.error_handling(XpressException)

    ''' Purpose: Sets the intital gap, and the gapincrement performed at each time interval if defined
        gapincrement is in percent, interval in seconds. Initial gap defaults to xpress default value'''
    def set_gap(self):
        start_time = time.time()
        if type(self.initial_gap) is float or type(self.initial_gap) is int:
            self.problem.setControl('miprelstop', self.initial_gap)

        # input validation
        if (type(self.gap_increment) is int or type(self.gap_increment) is float) and (
                type(self.gap_interval) is int or type(self.gap_interval) is float):
            if self.gap_increment > 0 and self.gap_interval > 0:
                self.logger.info('Adding dynamic gap callback')
                callback = add_new_node_callback(time_val=start_time, gap=self.initial_gap,  interval=self.gap_interval,
                                                 increment=self.gap_increment)
                self.problem.addcbnewnode(callback, self.logger)

    @property
    def gap_control(self):
        return self.problem.getControl('miprelstop')

    @property
    def max_time(self):
        return self.problem.controls.maxtime

    @max_time.setter
    def max_time(self, max_time):
        if type(max_time) is int or type(max_time) is float:
            self.problem.controls.maxtime = max_time

    def error_handling(self, exception):
        error_code = self.problem.getAttrib(ERROR_CODE)
        if error_code == 0:
            msg = exception.__str__()
            error_code = 'Undef'
        else:
            msg = self.problem.getlasterror()

        raise XpressError(errorCode=error_code, msg=msg)

    @classmethod
    def get_sum(cls, exp):
        return xp.Sum(exp)

    def constraint(self, exp, name):
        return xp.constraint(exp, name = name)

    def add_objective_element(self, element):
        self.Objective_Elements.append(element)

    def set_objective(self):
        objective = xp.Sum([element for element in self.Objective_Elements])
        try:
            self.problem.setObjective(objective, sense=xp.minimize)
        except Exception as XpressException:
            self.error_handling(XpressException)

    """Purpose: Solve the model for the registered constraints and objective"""
    def solve(self):
        # Info object holding data from input
        try:
            self.problem.addcbintsol(integer_sol_callback, self.solution, 0)
            self.problem.addcbnewnode(stop_callback, self)
        except Exception as XpressException:
            self.error_handling(XpressException)

        self.problem.setControl('prepermute', 3)
        self.problem.setControl('prepermuteseed', self.random_seed)

        self.set_gap()
        start_time = time.time()
        try:
            self.problem.solve()
        except Exception as XpressException:
            self.error_handling(XpressException)

        stop_time = time.time()
        self.logger.info('Xpress solver stopped')
        self.solution.status = self.problem.getProbStatusString()
        self.logger.info('Solution status is: ' + self.solution.status)
        self.solution.set_solution_time(sol_time=stop_time - start_time)
        self.solution.gap_control = self.gap_control

        if not self.solution.sol_found:
            if not self.solution.feasible:
                for func in self.handle_infeasibility:
                    inf_information = func(self.problem)
                    self.solution.no_solution_info = inf_information
            return self.solution

        else:
            # notify variables with objective value
            self.logger.info('Extracting variable solution values')
            xpress_vars = [var.get_var() for var in self.variables]
            sol_values = []
            try:
                sol_values = self.problem.getSolution(xpress_vars)
            except Exception as xpressException:
                self.error_handling(xpressException)
            self.logger.info('Notifying variable objects')
            i = 0
            for variable in self.variables:
                variable.notify(val=sol_values[i])
                i+=1

            self.logger.info('Extracting objective value')
            self.solution.objective_value = self.problem.getObjVal()
            self.solution.gap_set(lb=self.problem.attributes.bestbound, ub=self.problem.attributes.mipobjval)

        for func in self.solution_listener:
            func()

        [var.clear_variable() for var in self.variables]
        return self.solution

    # Write problem matrix to file
    def write_problem(self):
        self.problem.write("ctr", "l")

    def get_solution(self):
        return self.solution

    def get_problem(self):
        return self.problem

    def print_problem(self):
        self.problem.write('prob', 'lp')

    @classmethod
    def add_bin_var(cls, name):
        var = xp.var(name=name, vartype=xp.binary)
        return var

    @classmethod
    def add_cont_var(cls, name, lower_bound=None, upper_bound=None):
        if lower_bound is None and upper_bound is None:
            var = xp.var(name=name)
        elif lower_bound is not None and upper_bound is None:
            var = xp.var(name=name, lb=lower_bound)
        elif lower_bound is None and upper_bound is not None:
            var = xp.var(name=name, ub=upper_bound)
        else:
            var = xp.var(name=name, lb=lower_bound, ub=upper_bound)
        return var

    @classmethod
    def add_int_var(cls, name, lower_bound=None, upper_bound=None):
        if lower_bound is None and upper_bound is None:
            var = xp.var(name=name, vartype=xp.integer)
        elif lower_bound is not None and upper_bound is None:
            var = xp.var(name=name, lb=lower_bound, vartype=xp.integer)
        elif lower_bound is None and upper_bound is not None:
            var = xp.var(name=name, ub=upper_bound, vartype=xp.integer)
        else:
            var = xp.var(name=name, lb=lower_bound, ub=upper_bound, vartype=xp.integer)
        return var


"""
Purpose: Check for system errors, error codes from xpress documentation
https://www.fico.com/fico-xpress-optimization/docs/latest/solver/optimizer/HTML/chapter9_sec_section9001.html"""


def is_system_error(e):
    error_code_list = [
        706,  # No memory to add sets
        251,  # Out of memory
        245,  # Not enough memory to presolve matrix
        120,  # Problem has to many rows, typically license problem
        114,  # Fatal error
        50,   # Inconsistent basis
        20,   # Insufficient memory for array
        52,   # Too many non-zero elements, typically license problem
        167,  # Failed to allocate memory of size <bytes>
        394,  # Network error
        419,  # Not enough space to store cuts
        459,  # Not enough memory for BB tree

    ]
    if e.ErrorCode in error_code_list:
        return True
    return False


""" Purpose: Variable class to store variable information. Wraps xpress variable in the var field.
Called by model object to register solution values after running the model"""


class Variable:
    def __init__(self, name='notdef'):
        self.solution_val = 0
        self.var = None
        self.VarType = None
        self.var_func = None
        self.added = False
        self.nameHolder = name

    def add_var(self):
        self.var = self.var_func(name=self.get_name())
        return self.var

    def set_name(self, name):
        self.nameHolder = name

    def create_bin_var(self, name):
        self.nameHolder = name
        self.var = Model.add_bin_var(name)
        self.added = False
        return self

    def create_cont_var(self, name, lower_bound=None, upper_bound=None):
        self.nameHolder = name
        self.var = Model.add_cont_var(name, lower_bound, upper_bound)
        self.added = False
        return self

    def create_int_var(self, name, lower_bound=None, upper_bound=None):
        self.nameHolder = name
        self.var = Model.add_int_var(name, lower_bound, upper_bound)
        self.added = False
        return self

    def __mul__(self, other):
        return self.get_var()*other

    def __rmul__(self, other):
        return self.__mul__(other)

    def __add__(self, other):
        a = self.get_var()
        b = 0 + other
        return a + b

    def __sub__(self, other):
        a = self.get_var()
        b = 0-other
        return a-b

    def __rsub__(self, other):
        return self.__sub__(other)

    def __radd__(self, other):
        return self.__add__(other)

    def get_name(self):
        return self.nameHolder

    def get_var(self):
        return self.var

    def notify(self, val):
        self.set_solution_val(val)

    def set_solution_val(self, val):
        self.solution_val = val

    def get_solution_val(self):
        return self.solution_val

    def clear_variable(self):
        self.var = None


""" Purpose: Store general solution information such as gap, running time and objective value.
Returned in model solve method"""


class Solution:
    SOL_FOUND = 'mip_solution'
    MIP_OPTIMAL = 'mip_optimal'
    NO_SOL = 'NO_SOL'
    MIP_INF = 'mip_infeas'
    LP_INF = 'lp_infeas'
    LP_OPTIMAL = 'lp_optimal'

    def __init__(self):
        self._status = ''
        self.status = Solution.NO_SOL
        self._objective_value = -1
        self._name = ''
        self.ub = -1
        self.lb = -1
        self._gap = -1
        self._solution_time = -1
        self.no_solution_info = None
        self._gap_control = 0
        self._get_no_sol_info = None

    @property
    def no_sol_information(self):
        return self._get_no_sol_info

    @no_sol_information.setter
    def no_sol_information(self, info):
        self._get_no_sol_info = info

    @property
    def gap_control(self):
        return self._gap_control

    @gap_control.setter
    def gap_control(self, gap_control):
        self._gap_control = gap_control

    @property
    def feasible(self):
        if self.status == Solution.MIP_INF or self.status == Solution.LP_INF:
            return False
        return True

    @property
    def objective_value(self):
        return self._objective_value

    @objective_value.setter
    def objective_value(self, obj_val):
        self._objective_value = obj_val

    @property
    def gap(self):
        return self._gap

    @property
    def solution_time(self):
        return self._solution_time

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        if not type(name) is str:
            raise Exception('Solution name must be of type string')
        self._name = name

    @property
    def no_sol(self):
        if self.status == Solution.NO_SOL:
            return True
        return False

    @property
    def sol_found(self):
        if self.status == Solution.SOL_FOUND or self.status == Solution.MIP_OPTIMAL \
                or self.status == Solution.LP_OPTIMAL:
            return True
        return False

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    def gap_set(self, lb, ub):
        if lb == 0:
            self._gap = 0
        else:
            self._gap = abs((ub - lb) / ub)

    def get_solution_time(self):
        return self._solution_time

    def set_solution_time(self, sol_time):
        self._solution_time = sol_time


""" Purpose: Check xpress license """


def check_licence():
    adapter = LoggerAdapter(logger, None)
    try:
        adapter.info("Checking license")
        adapter.info(xp.getbanner())
    except RuntimeError:
        adapter.error("Unable to invoke xpress. Xpress license error message follows...")
        adapter.error(xp.getlicerrmsg())
        raise LicenseError(xp.getlicerrmsg())
