"""aFRR solver errors

All errors messages thrown by the marketsolver module should be encapsulated in exceptions defined here

"""
class LicenseError(Exception):
    """Raised when service is not able to acquire a license from Fico xPress"""
    pass

# TODO: Define necessary application errors, e.g.  NoSolutionFoundError, TimeoutError....



"""Raised for json request errors"""


class InputError(Exception):
    def __init__(self, msg):
        super(InputError, self).__init__(msg)


"""Raised when the objects are erroneously initialized"""


class InitializationError(InputError):
    def __init__(self, msg):
        super(InitializationError, self).__init__(msg)


"""Raised when the algorithm behaves wrongly, should not occur for a correctly initialized model"""


class ImplementationError(Exception):
    def __init__(self, msg):
        super(ImplementationError, self).__init__(msg)


class XpressError(Exception):
    def __init__(self, msg, errorCode):
        self.ErrorCode = errorCode
        self.Msg = msg

    def __str__(self):
        return str('Xpress error: ErrorCode: '+str(self.ErrorCode) + ", msg: "+str(self.Msg))

    def geterrorcode(self):
        return self.ErrorCode

    def geterrormsg(self):
        return self.Msg



class UserAbortError(Exception):
	"""Raised when a run is aborted by the user"""
	pass
