from .afrr_errors import InputError
from .data_objects import *
from .utilities import value_checker
import logging
from .loggers import *
logger = logging.getLogger(__name__)


class Database:
    """Purpose: Initialize objects with data from input"""
    def __init__(self, json_data=None, run_id=None):
        try:
            self.logger = LoggerAdapter(logger, run_id)
            self.stop_watch_logger = StopWatchAdapter(run_id)
            self.data = json_data
            self.regions = DataVector()
            self.macro_areas = DataVector()
            self.nordic_area = None
            self.optimality_gap = 0.0001
            self.gap_interval = 0
            self.gap_increment = None
            self.gap_interval = None
            self.random_seed = 0
            self.max_time = 600
            self.initialized = False
            if self.data is not None:
                self.initialize()
                self.initialized = True
        except KeyError as e:
            msg = 'Key error in json request'
            raise InputError(msg + str(e))
        except InputError as e:
            msg = 'JSON initialization failed'
            raise InputError(msg+str(e))

    def initialize(self):
        if type(self.data) is not dict:
            raise InputError('Data is not of type dict')
        self.logger.info('Staring initializing objects')
        self.stop_watch_logger.start()
        time_holder = self.init_time()
        direction_holder = self.init_direction()
        self.init_regions(time_holder, direction_holder)
        self.init_demand()
        self.init_macro_areas(time_holder, direction_holder)
        self.init_area_prod_constraints()
        self.init_nordic_area(time_holder, direction_holder)
        self.init_connections()
        self.init_bids()
        self.control_bid_ids()
        self.link_bids()
        self.exclusive_bids()
        self.init_optimization_parameters()
        self.stop_watch_logger.stop()
        self.stop_watch_logger.info('Initialized objects')

    def control_bid_ids(self):
        all_bids = self.nordic_area.all_bids
        self.logger.info('Controlling bids')
        ids = [bid.id for bid in self.nordic_area.all_bids]
        if len(all_bids) > len(set(ids)):
            msg = 'Bid id are not unique, cannot initialize optimization model'
            raise InputError(msg)
        self.logger.info('Done controlling bids')

    def init_optimization_parameters(self):
        self.logger.info('Initializing optimization parameters')

        def check_int_parameter(value):
            if type(value) is not int:
                raise InputError('Optimization parameter is not integer')
            return value

        def check_float_parameter(value):
            if type(value) is not int and type(value) is not float:
                raise InputError('Optimization parameter is not int or float')
            return value

        def check_int32_parameter(int_value):
            if abs(int_value) > 2 ** 31 - 1:
                raise InputError('Integer value is not int32')
            return int_value

        try:
            self.optimality_gap = check_float_parameter(self.data[CONFIG][OPTIMALITY_GAP])
        except InputError as e:
            self.logger.info('No optimality gap provided. Defaults to 0.0001. ' + str(e))
        except KeyError as e:
            self.logger.info('No optimality gap provided. Defaults to 0.0001. ' + str(e))

        try:
            self.max_time = check_int_parameter(self.data[CONFIG][MAX_TIME])
        except InputError as e:
            self.logger.info('No max time provided. Defaults to 600. ' + str(e))
        except KeyError as e:
            self.logger.info('No max time provided. Defaults to 600. ' + str(e))

        self.random_seed = value_checker(self.data[CONFIG][RANDOM_SEED], check_int_parameter, check_int32_parameter)

        try:
            self.gap_interval = check_float_parameter(self.data[CONFIG][GAP_INTERVAL])
            self.gap_increment = check_float_parameter(self.data[CONFIG][GAP_INCREMENT])
        except InputError as e:
            self.logger.info('Time dependent gap not initialized. '+str(e))
        except KeyError as e:
            self.logger.info('Time dependent gap not initialized. ' + str(e))

    def init_direction(self):
        directions = self.data[DIRECTIONS]
        up = -1
        down = -1
        for _dir in directions:
            if _dir[NAME] == UP:
                up = _dir[ID]
            elif _dir[NAME] == DOWN:
                down = _dir[ID]
            else:
                raise InputError('Direction code is not: '+str(UP)+' or '+str(DOWN))
        return DirectionHolder(up=up, down=down, up_code=UP, down_code=DOWN)

    def init_time(self):
        return TimeHolder(self.data[CONFIG][NHOURS])

    def init_nordic_area(self, time_holder, direction_holder):
        self.logger.info('Initializing nordic area')
        self.nordic_area = NordicArea(time_holder, direction_holder, self.regions, self.macro_areas,
                                      self.data[ID])
        return self.nordic_area

    def link_bids(self):
        self.logger.info('Initializing linked bids')
        # Create linked bids
        all_bids = self.nordic_area.all_bids.sort(lambda bid: bid.id)

        def _assert_consecutive_bids(_all_bids):
            i = 1
            for bid in _all_bids:
                if bid.id != i:
                    raise InputError('Bid ids are not consecutive')
                i += 1
        _assert_consecutive_bids(all_bids)

        for link in self.data[LINKED_BIDS]:
            linked_bids = DataVector()
            for bid_id in link:
                link_bid = all_bids[bid_id-1]
                linked_bids.add_object(link_bid)

            self.nordic_area.add_linked_bids(linked_bids)
        return self.nordic_area.linked_bids

    def exclusive_bids(self):
        self.logger.info('Initializing exclusive bids')
        all_bids = self.nordic_area.all_bids.sort(lambda bid: bid.id)
        for exclusive_list in self.data[EXCLUSIVE_BIDS]:
            exclusive_bids = DataVector()
            for bid_id in exclusive_list:
                exclusive_bid = all_bids[bid_id-1]
                exclusive_bids.add_object(exclusive_bid)
            self.nordic_area.add_exclusive_bids(exclusive_bids)
        return self.nordic_area.exclusive_bids

    def init_bids(self):
        self.logger.info('Initializing bids')
        # Initialize all bids
        bids = DataVector()
        quantity_factor = self.data[CONFIG][QUANTITY_FACTOR]
        i = 0
        for bid in self.data[BIDS]:
            bids.add_object(Bid(direction=bid[DIRECTION],
                                _id=bid[ID],
                                min_vol=bid[MIN_VOL],
                                region=self.get_region(bid[REGION]),
                                volume=bid[VOLUME],
                                price=bid[PRICE],
                                hour_from=bid[HOUR_FROM],
                                hour_to=bid[HOUR_TO],
                                quantity_factor=quantity_factor))
            i += 1
            if i % 1000 == 0:
                self.logger.info('Initialized ' + str(i) + ' bids')
        self.logger.info('Done initializing bids')
        return bids

    def init_connections(self):
        self.logger.info('Initializing connections')
        connections = self.data[CONNECTION_CAPACITY]
        connection_holder = DataVector()
        for connection in connections:
            from_region = self.get_region(connection[FROM_REGION])
            to_region = self.get_region(connection[TO_REGION])
            capacity = connection[CAPACITY]
            cost = connection[COST]
            hour = connection[HOUR]
            con = connection_holder.filter_object(lambda _connection: _connection.from_area == from_region
                                                  and _connection.to_area == to_region)

            if con is None:
                con = connection_holder.add_object(Connection(from_area=from_region, to_area=to_region))
            con.set_cost(hour, cost)
            con.set_capacity(hour, capacity)

            rev_con = connection_holder.filter_object(lambda _connection: _connection.from_area == to_region and
                                                                          _connection.to_area == from_region)
            # If revert connection does not exist. Create revert connection to assert link connection ctr
            if rev_con is None:
                connection_holder.add_object(Connection(from_area=to_region, to_area=from_region))

        return connections

    def get_region(self, _id):
        return self.regions.filter_object(lambda region: region.id == _id)

    def get_area(self, _id):
        return self.macro_areas.filter_object(lambda area: area.id == _id)

    def init_regions(self, time_holder, direction_holder):
        self.logger.info('Initializing regions')
        penalty = self.data[CONFIG][PROD_MIN_PENALTY_REGION]
        demand_penalty = self.data[CONFIG][DEMAND_PENALTY_REGION]
        for r in range(1, self.data[CONFIG][NREGIONS] + 1):
            self.regions.add_object(Region(r, time_holder, direction_holder, penalty, demand_penalty))
        return self.regions

    def init_demand(self):
        self.logger.info('Initializing demand in regions')
        for demand in self.data[AREA_DEMAND]:
            reg = self.get_region(demand[REGION])
            hour = demand[HOUR]
            direction = demand[DIRECTION]
            reg.set_demand(hour=hour, value=demand[DEMAND], direction=direction)
            try:
                reg.set_prod_min(hour=hour, value=demand[PROD_MIN], direction=direction)
                reg.set_prod_max(hour=hour, value=demand[PROD_MAX], direction=direction)
            except InputError as e:
                self.logger.warning('Cannot set production up limits. ' + str(e))

    def init_macro_areas(self, time_holder, direction_holder):
        self.logger.info('Initializing macro areas')
        penalty = self.data[CONFIG][PROD_MIN_PENALTY_MACRO]
        for i in range(1, self.data[CONFIG][NAREAS] + 1):
            self.macro_areas.add_object(MacroArea(i, time_holder, direction_holder, penalty))

        for area in self.data[MACRO_AREAS]:
            reg = self.get_region(area[REGION])
            m_area = self.get_area(area[MACRO_AREA])
            m_area.regions = reg

        return self.macro_areas

    def init_area_prod_constraints(self):
        self.logger.info('Initializing production min, max for macro areas')
        for demand in self.data[AREA_PROD_REST]:
            area = self.get_area(demand[MACRO_AREA])
            # Initialize production min and max in macro area
            try:
                area.set_prod_min(value=demand[PROD_MIN], hour=demand[HOUR], direction=demand[DIRECTION])
                area.set_prod_max(value=demand[PROD_MAX], hour=demand[HOUR], direction=demand[DIRECTION])
            except InputError as e:
                self.logger.warning('Cannot set production limits. ' + str(e))
