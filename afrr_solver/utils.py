import os
import json


termination_dir = './termination_data'

for f in ['preliminary_result', 'input', 'stop']:
    if not os.path.exists(os.path.join(termination_dir, f)):
        os.makedirs(os.path.join(termination_dir, f))


def get_path(run_id, data_type):
	run_id = str(run_id)
	return os.path.join(termination_dir, data_type, run_id)


def save_input(run_id, data):
	path = get_path(run_id, 'input')
	f = open(path, 'w+')
	f.write(json.dumps(data))
	f.close()


def read_input(run_id):
	path = get_path(run_id, 'input')
	try:
		f = open(path, 'r')
		data = json.loads(f.read())
		f.close()
		return data
	except:
		return None


def save_preliminary_result(run_id, data):
	path = get_path(run_id, 'preliminary_result')
	f = open(path, 'w+')
	f.write(json.dumps(data))
	f.close()


def read_preliminary_result(run_id):
	path = get_path(run_id, 'preliminary_result')
	try:
		f = open(path, 'r')
		data = json.loads(f.read())
		f.close()
		os.remove(path)
		return data
	except:
		return None


def save_stop(run_id):
	path = get_path(run_id, 'stop')
	f = open(path, 'w+')
	f.write("")
	f.close()


def read_stop(run_id):
	path = get_path(run_id, 'stop')
	should_stop = os.path.isfile(path)
	if should_stop:
		os.remove(path)
	return should_stop
	# try:
	# 	f = open(path, 'r')
	# 	data = json.loads(f.read())
	# 	f.close()
	# 	return True
	# except:
	# 	return False	

