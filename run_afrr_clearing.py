
import os

from afrr_solver.marketsolver import ModelClearing
from afrr_solver.setup_logging import setup_logging

import json

testSet1 = [
    #'retest_2_afrr-3281_input.json',
    #'retest_afrr-2943_input.json',
    #'input_2.json',
    #'input3.json',
    #'rel_inp_2.json',
    'current_input.json',
    #'input_4.json'

]

def write_json(sample, filename):
    with open(filename+'.json', 'w') as fp:
        json.dump(sample, fp)

def openjson(jsonfile):
    """openjson reads and stores the jsonfile as(data)"""

    try:
        with open(jsonfile) as f:
            data = json.load(f)

        return data
    except:
        raise Exception

def run():
    path = os.getcwd()
    for test in testSet1:

        os.chdir(path+'/testdata')
        data = openjson(test)
        os.chdir(path+'/afrr_solver')
        setup_logging()
        model = ModelClearing(1, data)
        model.initialize()
        model.set_up()
        print(model.clearmarket())

       # t2 = threading.Thread(target=worker2, args=(model,))
        #t2.start()


if __name__ == '__main__':
    run()

