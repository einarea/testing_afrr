# coding=utf-8
#
# Wrapper for å importere afrr-solver pakken og starte web-tjenesten
#
# Denne leveres i zip artefaktet og kalles fra afrr-solver.sh, som benyttes av drift til å
# starte/stoppe afrr-solver med riktig monitorering og logging
#
from afrr_solver.web import start_service

start_service()