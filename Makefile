#
# This makefile is identical for afrr-solver and fcr-solver projects, keep it that way!
# Remarks:
#   This makefile determines the artefact (fcr or afrr) from the git remote origin,
#   thus this makefile can only be used from a proper git repository for afrr/fcr
#
# The variable for next version is central to everything here.
# If the variable is undefined it is derived from git tags
NEXT_VERSION = $(shell source bash/release.sh; next_release)

ARTEFACT_BASE_NAME = $(shell git remote -v | grep -E "^origin.*(fetch)" | sed 's/.*\(\(fcr\|afrr\)-solver\).*/\1/')
PACKAGE_NAME = $(shell echo $(ARTEFACT_BASE_NAME) | tr "-" "_")
ARTEFACT_NAME = $(ARTEFACT_BASE_NAME)-$(NEXT_VERSION)
VENV = $(HOME)/.virtualenvs/$(ARTEFACT_NAME)
DIST_DIR = dist/$(ARTEFACT_NAME)
ZIP_NAME = $(ARTEFACT_NAME).zip
HASH_NAME= $(ARTEFACT_NAME).sha256
NEXUS_FIFTY_RELEASES_URL = https://repository.larm.statnett.no/repository/mms-releases
NEXUS_TARGET_DIR = $(NEXUS_FIFTY_RELEASES_URL)/fifty/optimeering/$(ARTEFACT_BASE_NAME)/$(NEXT_VERSION)
NEXUS_TARGET_ZIP_URL=$(NEXUS_TARGET_DIR)/$(ZIP_NAME)
NEXUS_TARGET_HASH_URL=$(NEXUS_TARGET_DIR)/$(HASH_NAME)
MAKE := $(MAKE) --no-print-directory
ARTIFACTORY_PYPI_UPLOAD_URL = https://artifactory.statnett.no/artifactory/api/pypi/pypi_statnett_ia_virtual
ARTIFACTORY_PYPI_INSTALL_URL = $(ARTIFACTORY_PYPI_UPLOAD_URL)/simple
SHELL = bash

default:
	@echo
	@echo 'Usage:'
	@echo "Makefile for $(PACKAGE_NAME)"
	@echo
	@echo "    make virtual    creates virtual environment under $(VENV)'
	@echo '    make devprep    prepares virtual environment and installs requirements to $(VENV)'
	@echo '    make zip        invokes setup and creates zip file with all wheels for $(ARTEFACT_BASE_NAME) and dependencies'
	@echo '    make extlibs    uses pip to create wheel files for all third-party deps of $(ARTEFACT_BASE_NAME)'
	@echo '    make changelist shows list of commits related to algoritm changes since previous release'
	@echo '    make release    creates release, tags git, and publishes artifact to Nexus. To specify version: make NEXT_VERSION=1.2.3 release'
	@echo '    make clean      cleanup temporary files'
	@echo

test:
	@echo "NEXT_VERSION=$(NEXT_VERSION)"
	@echo "VENV=$(VENV)"
	@echo "NEXUS_TARGET_ZIP_URL=$(NEXUS_TARGET_ZIP_URL)"
	@echo "PACKAGE_NAME=$(PACKAGE_NAME)"

	@if [ ! -d "$(VENV)" ]; then echo "$(VENV) does not exist, run make virtual first"; exit 1; else echo "$(VENV) exists, continuing.."; fi
	source $(VENV)/bin/activate ; \
	pytest $(PACKAGE_NAME)/unittests.py

virtual:
	rm -rf $(VENV)
	virtualenv --python=python3.6 $(VENV)

changelist:
	@echo "Algoritm updates since last release: "
	@source bash/release.sh; show_algoritm_updates

devprep:
	@if [ ! -d "$(VENV)" ]; then echo "$(VENV) does not exist, run make virtual first"; exit 1; else echo "$(VENV) exists, continuing.."; fi
	source $(VENV)/bin/activate ; \
	pip install --index-url $(ARTIFACTORY_PYPI_INSTALL_URL) -r requirements.txt
	source $(VENV)/bin/activate ; \
	pip install --index-url $(ARTIFACTORY_PYPI_INSTALL_URL) pytest

extlibs: $(DIST_DIR)/wheelhouse/*whl

$(DIST_DIR)/wheelhouse/*whl:
	@if [ ! -d "$(VENV)" ]; then echo "$(VENV) does not exist, run make virtual first"; exit 1; else echo "$(VENV) exists, continuing.."; fi
	mkdir -p $(DIST_DIR)/wheelhouse
	rm -f $(DIST_DIR)/wheelhouse/*whl
	source $(VENV)/bin/activate ; \
	pip wheel --index-url $(ARTIFACTORY_PYPI_INSTALL_URL) -r requirements.txt  -w $(DIST_DIR)/wheelhouse

zip: extlibs $(ARTEFACT_BASE_NAME) dist/$(ZIP_NAME)

$(ARTEFACT_BASE_NAME): extlibs $(DIST_DIR)/$(PACKAGE_NAME)*.whl bash/solver.sh

$(DIST_DIR)/$(PACKAGE_NAME)*.whl:
	SOLVER_VERSION=$(NEXT_VERSION) python setup.py bdist_wheel --dist-dir $(DIST_DIR)
	cp solver-starter.py $(DIST_DIR)
	cp bash/solver.sh $(DIST_DIR)
	@echo "SOLVER_VERSION=$(NEXT_VERSION)" >> $(DIST_DIR)/solver.properties
	@echo "ENABLE_FLASK_THREADING=false" >> $(DIST_DIR)/solver.properties
	@echo "ENABLE_DEBUG=false" >> $(DIST_DIR)/solver.properties
	@echo "WERKZEUG_DEBUG_PIN=off" >> $(DIST_DIR)/solver.properties
	@echo "export SOLVER_VERSION ENABLE_FLASK_THREADING ENABLE_DEBUG WERKZEUG_DEBUG_PIN" >> $(DIST_DIR)/solver.properties

dist/$(ZIP_NAME):
	pushd dist; zip -9r $(ZIP_NAME) $(ARTEFACT_NAME); popd

release: clean zip
	@echo "Preparing release"
	source bash/release.sh; \
	prepare_release $(NEXT_VERSION)
	@echo
	@echo "Adding release notes to zip file"
	mv dist/release-notes.tmp dist/$(ARTEFACT_NAME)/RELEASE-NOTES.md
	pushd dist; zip -9u $(ZIP_NAME) $(ARTEFACT_NAME)/RELEASE-NOTES.md; popd
	pushd dist; sha256sum $(ZIP_NAME) > $(HASH_NAME); popd
	@echo "Uploading $(ZIP_NAME) to Nexus"
	$(eval NEXUS_USERNAME ?= $(shell bash -c 'read -p "Nexus username for upload : " user; echo $$user'))
	@echo
	$(eval NEXUS_PASSWORD ?= $(shell bash -c 'read -s -p "Password : " pwd; echo $$pwd'))
	curl -v --progress-bar -u $(NEXUS_USERNAME):$(NEXUS_PASSWORD) --upload-file dist/$(ZIP_NAME) $(NEXUS_TARGET_ZIP_URL) | tee -a /tmp/progress.txt
	curl -v --progress-bar -u $(NEXUS_USERNAME):$(NEXUS_PASSWORD) --upload-file dist/$(HASH_NAME) $(NEXUS_TARGET_HASH_URL) | tee -a /tmp/progress.txt
	@echo
	source bash/release.sh; \
	finalize_release
	@echo "Release successful"

clean:
	@rm -Rf *.egg *.egg-info .cache build dist temp $(PACKAGE_NAME)*whl $(ARTEFACT_BASE_NAME)*zip $(ZIP_NAME)
	@find -depth -type d -name __pycache__ -exec rm -Rf {} \;
	@find -type f -name '*.pyc' -delete

.PHONY: default virtual extlibs $(ARTEFACT_BASE_NAME) zip release clean devprep test changelist

